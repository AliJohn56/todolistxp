"use strict";
/**
* Styles.tsx
* Copyright: Microsoft 2018
*
* Shared style information used throughout the application.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var fonts_1 = require("modules/fonts");
var RX = require("reactxp");
// Font infos
var Fonts = /** @class */ (function () {
    function Fonts() {
    }
    Fonts.monospace = {
        fontFamily: fonts_1.default.monospace
    };
    Fonts.displayLight = {
        fontFamily: fonts_1.default.displayLight
    };
    Fonts.displayRegular = {
        fontFamily: fonts_1.default.displayRegular,
        fontWeight: '400'
    };
    Fonts.displaySemibold = {
        fontFamily: fonts_1.default.displaySemibold,
        fontWeight: '600'
    };
    Fonts.displayBold = {
        fontFamily: fonts_1.default.displayBold,
        fontWeight: '700'
    };
    return Fonts;
}());
exports.Fonts = Fonts;
// Styles
var Styles = /** @class */ (function () {
    function Styles() {
    }
    Styles.statusBarTopMargin = RX.Styles.createViewStyle({
        marginTop: RX.StatusBar.isOverlay() ? 20 : 0
    });
    return Styles;
}());
exports.Styles = Styles;

//# sourceMappingURL=Styles.js.map
