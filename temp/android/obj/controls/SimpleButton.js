"use strict";
/*
* SimpleButton.tsx
* Copyright: Microsoft 2018
*
* A "classic" button with text as a label.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var HoverButton_1 = require("./HoverButton");
var Styles_1 = require("../app/Styles");
var _styles = {
    button: RX.Styles.createViewStyle({
        borderWidth: 1,
        borderRadius: 8,
        paddingVertical: 4,
        paddingHorizontal: 12,
        backgroundColor: "#fff" /* simpleButtonBackground */,
        borderColor: "#999" /* simpleButtonBorder */
    }),
    buttonHover: RX.Styles.createViewStyle({
        backgroundColor: "#eee" /* simpleButtonBackgroundHover */
    }),
    text: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        color: "#666" /* simpleButtonText */
    }),
    textHover: RX.Styles.createTextStyle({
        color: "#000" /* simpleButtonTextHover */
    })
};
var SimpleButton = /** @class */ (function (_super) {
    tslib_1.__extends(SimpleButton, _super);
    function SimpleButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onRenderButton = function (isHovering) {
            var buttonStyles = [_styles.button, _this.props.buttonStyle];
            var textStyles = [_styles.text, _this.props.textStyle];
            if (isHovering) {
                buttonStyles.push(_styles.buttonHover);
                buttonStyles.push(_this.props.buttonHoverStyle);
                textStyles.push(_styles.textHover);
                textStyles.push(_this.props.textHoverStyle);
            }
            return (RX.createElement(RX.View, { style: buttonStyles },
                RX.createElement(RX.Text, { style: textStyles }, _this.props.text)));
        };
        return _this;
    }
    SimpleButton.prototype.render = function () {
        return (RX.createElement(HoverButton_1.default, { title: this.props.title, disabled: this.props.disabled, onPress: this.props.onPress, onRenderChild: this._onRenderButton }));
    };
    return SimpleButton;
}(RX.Component));
exports.default = SimpleButton;

//# sourceMappingURL=SimpleButton.js.map
