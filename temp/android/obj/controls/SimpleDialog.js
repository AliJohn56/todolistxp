"use strict";
/**
* SimpleDialog.tsx
* Copyright: Microsoft 2018
*
* Defines the contents (including a title and buttons) for
* a dialog box. Typically embedded within an RX.Modal component.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var _ = require("lodash");
var RX = require("reactxp");
var resub_1 = require("resub");
var KeyCodes_1 = require("../utilities/KeyCodes");
var Modal_1 = require("./Modal");
var SimpleButton_1 = require("./SimpleButton");
var Styles_1 = require("../app/Styles");
var _modalPadding = 16;
var _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        backgroundColor: "#fff" /* simpleDialogBackground */,
        borderWidth: 1,
        borderColor: "#ddd" /* simpleDialogBorder */
    }),
    titleText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displaySemibold,
        fontSize: 16 /* size16 */,
        color: "#666" /* simpleDialogText */,
        textAlign: 'center',
        paddingVertical: _modalPadding
    }),
    contentText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        color: "#666" /* simpleDialogText */,
        textAlign: 'left',
        paddingVertical: _modalPadding
    }),
    contentContainer: RX.Styles.createViewStyle({
        flex: 1,
        paddingHorizontal: _modalPadding
    }),
    buttonContainer: RX.Styles.createViewStyle({
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: _modalPadding
    }),
    button: RX.Styles.createViewStyle({
        marginLeft: 12
    }),
    panelHeader: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayBold,
        fontSize: 16 /* size16 */,
        color: "#666" /* gray66 */
    }),
    displayText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        color: "#666" /* gray66 */
    })
};
var SimpleDialog = /** @class */ (function (_super) {
    tslib_1.__extends(SimpleDialog, _super);
    function SimpleDialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._renderButton = function (buttonDef, index) {
            return (RX.createElement(SimpleButton_1.default, { key: index, onPress: function (e) { return _this._onButtonPress(e, buttonDef); }, title: buttonDef.text, text: buttonDef.text || '', disabled: buttonDef.isDisabled, buttonStyle: [_styles.button, buttonDef.buttonStyle], textStyle: buttonDef.textStyle }));
        };
        _this._onKeyUp = function (event) {
            var buttonToCall;
            if (event.keyCode === KeyCodes_1.default.Escape) {
                _.each(_this.props.buttons, function (button) {
                    if (button.isCancel) {
                        buttonToCall = button;
                    }
                });
                if (buttonToCall) {
                    _this._completeButtonPress(buttonToCall);
                    return true;
                }
            }
            return false;
        };
        _this._onBack = function () {
            var buttonToCall;
            // Map the hardware back button to "cancel".
            _.each(_this.props.buttons, function (button) {
                if (button.isCancel) {
                    buttonToCall = button;
                }
            });
            if (buttonToCall) {
                _this._completeButtonPress(buttonToCall);
                return true;
            }
            return false;
        };
        return _this;
    }
    SimpleDialog.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        RX.Input.backButtonEvent.subscribe(this._onBack);
        RX.Input.keyUpEvent.subscribe(this._onKeyUp);
    };
    SimpleDialog.prototype.componentWillUnmount = function () {
        _super.prototype.componentWillUnmount.call(this);
        RX.Input.backButtonEvent.unsubscribe(this._onBack);
        RX.Input.keyUpEvent.unsubscribe(this._onKeyUp);
    };
    SimpleDialog.prototype.render = function () {
        // Title Text
        var optionalTitleText;
        if (this.props.title) {
            optionalTitleText = (RX.createElement(RX.View, { importantForAccessibility: RX.Types.ImportantForAccessibility.Yes },
                RX.createElement(RX.Text, { style: [_styles.panelHeader, _styles.titleText], importantForAccessibility: RX.Types.ImportantForAccessibility.NoHideDescendants }, this.props.title)));
        }
        // Content (children)
        var optionalContent;
        if (this.props.children) {
            optionalContent = this.props.children;
        }
        else if (this.props.text) {
            optionalContent = (RX.createElement(RX.Text, { style: [_styles.displayText, _styles.contentText] }, this.props.text));
        }
        // Buttons
        var optionalButtonsContainer;
        if (this.props.buttons && this.props.buttons.length > 0) {
            var optionalButtons = _.map(this.props.buttons, this._renderButton);
            optionalButtonsContainer = (RX.createElement(RX.View, { style: _styles.buttonContainer }, optionalButtons));
        }
        return (RX.createElement(Modal_1.default, { modalId: this.props.dialogId, modalWidth: this.props.maxWidth || 450, modalHeight: this.props.maxHeight },
            RX.createElement(RX.View, { style: [_styles.container, this.props.containerStyle] },
                optionalTitleText,
                RX.createElement(RX.View, { style: _styles.contentContainer }, optionalContent),
                optionalButtonsContainer)));
    };
    SimpleDialog.prototype._onButtonPress = function (e, buttonDef) {
        e.stopPropagation();
        this._completeButtonPress(buttonDef);
    };
    SimpleDialog.prototype._completeButtonPress = function (buttonDef) {
        if (buttonDef.onPress) {
            buttonDef.onPress();
        }
    };
    SimpleDialog.dismissAnimated = function (dialogId) {
        return Modal_1.default.dismissAnimated(dialogId);
    };
    return SimpleDialog;
}(resub_1.ComponentBase));
exports.default = SimpleDialog;

//# sourceMappingURL=SimpleDialog.js.map
