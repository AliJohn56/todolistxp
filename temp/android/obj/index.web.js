"use strict";
/**
* index.web.tsx
* Copyright: Microsoft 2018
*
* Javascript main entry point for web app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var AppBootstrapperWeb_1 = require("./app/AppBootstrapperWeb");
// This prevents bundlers/optimizers from stripping out the import above.
if (AppBootstrapperWeb_1.default) {
    console.log('App started');
}

//# sourceMappingURL=index.web.js.map
