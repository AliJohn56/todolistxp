"use strict";
/**
* index.native.ts
* Copyright: Microsoft 2018
*
* Native implementation of "fonts" module.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var Fonts = /** @class */ (function () {
    function Fonts() {
        this.monospace = 'System';
        this.displayLight = 'System';
        this.displayRegular = 'System';
        this.displaySemibold = 'System';
        this.displayBold = 'System';
    }
    return Fonts;
}());
exports.default = new Fonts();

//# sourceMappingURL=index.native.js.map
