"use strict";
/**
* ServiceRegistrar.tsx
* Copyright: Microsoft 2018
*
* Registers all services (long-running singleton objects) with the
* ServiceManager. Should be called at app launch time.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var AppConfig_1 = require("../app/AppConfig");
var PageUrlService_1 = require("./PageUrlService");
var ServiceManager_1 = require("./ServiceManager");
var TodosStore_1 = require("../stores/TodosStore");
var ServiceRegistrar = /** @class */ (function () {
    function ServiceRegistrar() {
    }
    ServiceRegistrar.init = function () {
        ServiceManager_1.default.registerService(TodosStore_1.default, 'TodosStore');
        // Web-specific services
        if (AppConfig_1.default.getPlatformType() === 'web') {
            ServiceManager_1.default.registerService(PageUrlService_1.default, 'PageUrlService');
        }
    };
    return ServiceRegistrar;
}());
exports.default = ServiceRegistrar;

//# sourceMappingURL=ServiceRegistrar.js.map
