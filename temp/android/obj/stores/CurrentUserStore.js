"use strict";
/**
* CurrentUserStore.tsx
* Copyright: Microsoft 2018
*
* Singleton store that maintains information about the currently-signed-in user.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var resub_1 = require("resub");
var CurrentUserStore = /** @class */ (function (_super) {
    tslib_1.__extends(CurrentUserStore, _super);
    function CurrentUserStore() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // TODO - properly initialize
        _this._user = {
            id: '1',
            fullName: 'Adam Smith',
            email: 'adam.smith@sample.com'
        };
        return _this;
    }
    CurrentUserStore.prototype.getUser = function () {
        return this._user;
    };
    CurrentUserStore.prototype.getFullName = function () {
        return this._user ? this._user.fullName : '';
    };
    tslib_1.__decorate([
        resub_1.autoSubscribe
    ], CurrentUserStore.prototype, "getUser", null);
    tslib_1.__decorate([
        resub_1.autoSubscribe
    ], CurrentUserStore.prototype, "getFullName", null);
    CurrentUserStore = tslib_1.__decorate([
        resub_1.AutoSubscribeStore
    ], CurrentUserStore);
    return CurrentUserStore;
}(resub_1.StoreBase));
exports.CurrentUserStore = CurrentUserStore;
exports.default = new CurrentUserStore();

//# sourceMappingURL=CurrentUserStore.js.map
