"use strict";
/**
* ResponsiveWidthStore.ts
* Copyright: Microsoft 2018
*
* Singleton store to hold the responsive width property for the app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var resub_1 = require("resub");
var ResponsiveWidthModels_1 = require("../models/ResponsiveWidthModels");
var TriggerKeys;
(function (TriggerKeys) {
    TriggerKeys[TriggerKeys["ResponsiveWidth"] = 0] = "ResponsiveWidth";
    TriggerKeys[TriggerKeys["Width"] = 1] = "Width";
    TriggerKeys[TriggerKeys["Height"] = 2] = "Height";
})(TriggerKeys = exports.TriggerKeys || (exports.TriggerKeys = {}));
var MainWindowId = 'MainWindowId';
var ResponsiveWidthStore = /** @class */ (function (_super) {
    tslib_1.__extends(ResponsiveWidthStore, _super);
    function ResponsiveWidthStore() {
        var _a, _b, _c;
        var _this = _super.call(this) || this;
        _this._rawWidth = (_a = {}, _a[MainWindowId] = 0, _a);
        _this._rawHeight = (_b = {}, _b[MainWindowId] = 0, _b);
        _this._responsiveWidth = (_c = {}, _c[MainWindowId] = ResponsiveWidthModels_1.ResponsiveWidth.Medium, _c);
        // Conditionally seed the window size when it isn't already set.
        if (!_this._rawWidth[MainWindowId] || !_this._rawHeight[MainWindowId]) {
            var _d = RX.UserInterface.measureWindow(), width = _d.width, height = _d.height;
            _this.putWindowSize(width, height);
        }
        return _this;
    }
    ResponsiveWidthStore_1 = ResponsiveWidthStore;
    ResponsiveWidthStore.responsiveWidthForWidth = function (width) {
        if (width < ResponsiveWidthModels_1.WidthBreakPoints.tiny) {
            return ResponsiveWidthModels_1.ResponsiveWidth.Tiny;
        }
        else if (width >= ResponsiveWidthModels_1.WidthBreakPoints.tiny && width < ResponsiveWidthModels_1.WidthBreakPoints.small) {
            return ResponsiveWidthModels_1.ResponsiveWidth.Small;
        }
        else if (width >= ResponsiveWidthModels_1.WidthBreakPoints.small && width < ResponsiveWidthModels_1.WidthBreakPoints.medium) {
            return ResponsiveWidthModels_1.ResponsiveWidth.Medium;
        }
        else {
            return ResponsiveWidthModels_1.ResponsiveWidth.Large;
        }
    };
    ResponsiveWidthStore.prototype.putWindowSize = function (width, height, rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        var triggers = [];
        // No need to re-calc when the width is unchanged
        var widthUpdated = this._rawWidth[rootViewId] !== width;
        if (widthUpdated) {
            this._rawWidth[rootViewId] = width;
            triggers.push(TriggerKeys.Width);
            var responsiveWidth = ResponsiveWidthStore_1.responsiveWidthForWidth(width);
            if (this._responsiveWidth[rootViewId] !== responsiveWidth) {
                this._responsiveWidth[rootViewId] = responsiveWidth;
                triggers.push(TriggerKeys.ResponsiveWidth);
            }
        }
        // No need to re-calc when the height is unchanged
        if (this._rawHeight[rootViewId] !== height) {
            this._rawHeight[rootViewId] = height;
            triggers.push(TriggerKeys.Height);
        }
        // Trigger all changes at once.
        this.trigger(triggers);
    };
    ResponsiveWidthStore.prototype.getWidth = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._rawWidth[rootViewId];
    };
    ResponsiveWidthStore.prototype.getWidthNoSubscription = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._rawWidth[rootViewId];
    };
    ResponsiveWidthStore.prototype.getHeight = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._rawHeight[rootViewId];
    };
    ResponsiveWidthStore.prototype.getHeightNoSubscription = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._rawHeight[rootViewId];
    };
    ResponsiveWidthStore.prototype.getResponsiveWidth = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._responsiveWidth[rootViewId];
    };
    ResponsiveWidthStore.prototype.isSmallOrTinyScreenSize = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._responsiveWidth[rootViewId] <= ResponsiveWidthModels_1.ResponsiveWidth.Small;
    };
    ResponsiveWidthStore.prototype.isTinyWidth = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return this._responsiveWidth[rootViewId] <= ResponsiveWidthModels_1.ResponsiveWidth.Tiny;
    };
    ResponsiveWidthStore.prototype.isHeightSmallerThanThresholdNoSubscription = function (threshold, rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        var size = this.getWindowDimensionsNoSubscription(rootViewId);
        return size.height <= threshold;
    };
    ResponsiveWidthStore.prototype.isWidthSmallerThanThresholdNoSubscription = function (threshold, rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        var size = this.getWindowDimensionsNoSubscription(rootViewId);
        return size.width <= threshold;
    };
    ResponsiveWidthStore.prototype.getWindowDimensionsNoSubscription = function (rootViewId) {
        if (rootViewId === void 0) { rootViewId = MainWindowId; }
        return RX.UserInterface.measureWindow(rootViewId === MainWindowId ? undefined : rootViewId);
    };
    var ResponsiveWidthStore_1;
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], ResponsiveWidthStore.prototype, "putWindowSize", null);
    tslib_1.__decorate([
        resub_1.autoSubscribeWithKey(TriggerKeys.Width)
    ], ResponsiveWidthStore.prototype, "getWidth", null);
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], ResponsiveWidthStore.prototype, "getWidthNoSubscription", null);
    tslib_1.__decorate([
        resub_1.autoSubscribeWithKey(TriggerKeys.Height)
    ], ResponsiveWidthStore.prototype, "getHeight", null);
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], ResponsiveWidthStore.prototype, "getHeightNoSubscription", null);
    tslib_1.__decorate([
        resub_1.autoSubscribeWithKey(TriggerKeys.ResponsiveWidth)
    ], ResponsiveWidthStore.prototype, "getResponsiveWidth", null);
    tslib_1.__decorate([
        resub_1.autoSubscribeWithKey(TriggerKeys.ResponsiveWidth)
    ], ResponsiveWidthStore.prototype, "isSmallOrTinyScreenSize", null);
    tslib_1.__decorate([
        resub_1.autoSubscribeWithKey(TriggerKeys.ResponsiveWidth)
    ], ResponsiveWidthStore.prototype, "isTinyWidth", null);
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], ResponsiveWidthStore.prototype, "isHeightSmallerThanThresholdNoSubscription", null);
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], ResponsiveWidthStore.prototype, "isWidthSmallerThanThresholdNoSubscription", null);
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], ResponsiveWidthStore.prototype, "getWindowDimensionsNoSubscription", null);
    ResponsiveWidthStore = ResponsiveWidthStore_1 = tslib_1.__decorate([
        resub_1.AutoSubscribeStore
    ], ResponsiveWidthStore);
    return ResponsiveWidthStore;
}(resub_1.StoreBase));
exports.default = new ResponsiveWidthStore();

//# sourceMappingURL=ResponsiveWidthStore.js.map
