"use strict";
/*
* TodoListItem.tsx
* Copyright: Microsoft 2018
*
* Renders a list item that represents a todo item.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var images_1 = require("modules/images");
var RX = require("reactxp");
var resub_1 = require("resub");
var HoverButton_1 = require("../controls/HoverButton");
var Styles_1 = require("../app/Styles");
var _itemBorderWidth = 1;
var _styles = {
    container: RX.Styles.createButtonStyle({
        alignSelf: 'stretch',
        borderBottomWidth: _itemBorderWidth,
        borderColor: "#eee" /* borderSeparatorLight */,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: "#fff" /* white */
    }),
    todoNameText: RX.Styles.createTextStyle({
        flex: -1,
        fontSize: 16 /* size16 */,
        font: Styles_1.Fonts.displayRegular,
        color: "#666" /* menuText */,
        margin: 8
    }),
    todoNameTextSelected: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displaySemibold,
        color: "#225577" /* menuTextSelected */
    }),
    todoImage: RX.Styles.createImageStyle({
        marginLeft: 16,
        marginRight: 4,
        height: 20,
        width: 24
    }),
    hovering: RX.Styles.createButtonStyle({
        backgroundColor: "#f8f8ff" /* listItemHover */
    }),
    selected: RX.Styles.createButtonStyle({
        backgroundColor: "#ddeeff" /* listItemSelected */
    })
};
var TodoListItem = /** @class */ (function (_super) {
    tslib_1.__extends(TodoListItem, _super);
    function TodoListItem() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onPress = function (e) {
            // Prevent VirtualListView.onItemSelected from
            // being triggering in the web app.
            e.stopPropagation();
            _this.props.onPress(_this.props.todo.id);
        };
        _this._onRenderItem = function (isHovering) {
            var buttonStyles = [_styles.container, _this.state.heightStyle];
            if (_this.props.isSelected) {
                buttonStyles.push(_styles.selected);
            }
            else if (isHovering) {
                buttonStyles.push(_styles.hovering);
            }
            var nameText;
            var searchString = _this.props.searchString ? _this.props.searchString.trim().toLowerCase() : '';
            var searchSubstrIndex = -1;
            if (searchString) {
                searchSubstrIndex = _this.props.todo.text.toLowerCase().indexOf(searchString);
            }
            if (searchSubstrIndex >= 0) {
                nameText = (RX.createElement(RX.Text, { style: _styles.todoNameText, numberOfLines: 1 },
                    RX.createElement(RX.Text, { numberOfLines: 1 }, _this.props.todo.text.substr(0, searchSubstrIndex)),
                    RX.createElement(RX.Text, { style: _styles.todoNameTextSelected, numberOfLines: 1 }, _this.props.todo.text.substr(searchSubstrIndex, searchString.length)),
                    RX.createElement(RX.Text, { numberOfLines: 1 }, _this.props.todo.text.substr(searchSubstrIndex + searchString.length))));
            }
            else {
                nameText = (RX.createElement(RX.Text, { style: _styles.todoNameText, numberOfLines: 1 }, _this.props.todo.text));
            }
            return (RX.createElement(RX.View, { style: buttonStyles },
                RX.createElement(RX.Image, { style: _styles.todoImage, source: images_1.default.todoSmall }),
                nameText));
        };
        return _this;
    }
    TodoListItem.prototype._buildState = function (props, initState) {
        var partialState = {
            heightStyle: RX.Styles.createViewStyle({
                height: props.height
            }, false)
        };
        return partialState;
    };
    TodoListItem.prototype.render = function () {
        return (RX.createElement(HoverButton_1.default, { onRenderChild: this._onRenderItem, onPress: this._onPress }));
    };
    return TodoListItem;
}(resub_1.ComponentBase));
exports.default = TodoListItem;

//# sourceMappingURL=TodoListItem.js.map
