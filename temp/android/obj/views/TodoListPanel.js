"use strict";
/**
* TodoListPanel.tsx
* Copyright: Microsoft 2018
*
* Display first screen of the Todo application.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var _ = require("lodash");
var RX = require("reactxp");
var reactxp_virtuallistview_1 = require("reactxp-virtuallistview");
var resub_1 = require("resub");
var AppConfig_1 = require("../app/AppConfig");
var HoverButton_1 = require("../controls/HoverButton");
var Styles_1 = require("../app/Styles");
var TodoListItem_1 = require("./TodoListItem");
var TodosStore_1 = require("../stores/TodosStore");
var _listItemHeight = 48;
var _styles = {
    listScroll: RX.Styles.createViewStyle({
        flexDirection: 'column',
        alignSelf: 'stretch',
        backgroundColor: "white" /* contentBackground */
    }),
    todoListHeader: RX.Styles.createViewStyle({
        height: 60,
        borderBottomWidth: 1,
        borderColor: "#ccc" /* borderSeparator */,
        flexDirection: 'row',
        alignItems: 'center'
    }),
    searchBox: RX.Styles.createTextInputStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 14 /* size14 */,
        borderWidth: 1,
        borderColor: "#ccc" /* borderSeparator */,
        flex: 1,
        padding: 4,
        marginLeft: 12
    }),
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: "white" /* contentBackground */
    }),
    addTodoButton: RX.Styles.createViewStyle({
        margin: 8,
        paddingHorizontal: 8,
        paddingVertical: 4
    }),
    buttonText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 32 /* size32 */,
        lineHeight: 32,
        color: "#666" /* buttonTextColor */
    }),
    buttonTextHover: RX.Styles.createTextStyle({
        color: "#000" /* buttonTextHover */
    })
};
var TodoListPanel = /** @class */ (function (_super) {
    tslib_1.__extends(TodoListPanel, _super);
    function TodoListPanel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onRenderAddTodoButton = function (isHovering) {
            return (RX.createElement(RX.View, { style: _styles.addTodoButton },
                RX.createElement(RX.Text, { style: [_styles.buttonText, isHovering ? _styles.buttonTextHover : undefined] }, '+')));
        };
        _this._onChangeTextSearch = function (newValue) {
            var filteredTodoList = _this._filterTodoList(_this.state.todos, newValue.trim());
            _this.setState({
                filteredTodoList: filteredTodoList,
                searchString: newValue
            });
        };
        _this._renderItem = function (details) {
            var item = details.item;
            return (RX.createElement(TodoListItem_1.default, { todo: item.todo, height: _listItemHeight, isSelected: item.todo.id === _this.props.selectedTodoId, searchString: _this.state.searchString, onPress: _this._onPressTodo }));
        };
        _this._onPressTodo = function (todoId) {
            _this.props.onSelect(todoId);
            _this.setState({
                searchString: '',
                filteredTodoList: _this.state.todos
            });
        };
        _this._onPressCreateNewTodo = function () {
            _this.props.onCreateNew();
            _this.setState({
                searchString: '',
                filteredTodoList: _this.state.todos
            });
        };
        return _this;
    }
    TodoListPanel.prototype._buildState = function (props, initState) {
        var partialState = {};
        partialState.todos = TodosStore_1.default.getTodos().map(function (todo, i) {
            return {
                key: i.toString(),
                height: _listItemHeight,
                template: 'todo',
                todo: todo
            };
        });
        if (initState) {
            partialState.searchString = '';
            partialState.filteredTodoList = partialState.todos;
        }
        else {
            var filter = _.trim(partialState.searchString);
            if (filter) {
                partialState.filteredTodoList = this._filterTodoList(partialState.todos, filter);
            }
            else {
                partialState.filteredTodoList = partialState.todos;
            }
        }
        return partialState;
    };
    TodoListPanel.prototype.render = function () {
        return (RX.createElement(RX.View, { useSafeInsets: true, style: _styles.container },
            RX.createElement(RX.View, { style: _styles.todoListHeader },
                RX.createElement(RX.TextInput, { style: _styles.searchBox, value: this.state.searchString, autoFocus: !AppConfig_1.default.isTouchInterface(), placeholder: 'Search', onChangeText: this._onChangeTextSearch, autoCapitalize: 'none' }),
                RX.createElement(HoverButton_1.default, { onPress: this._onPressCreateNewTodo, onRenderChild: this._onRenderAddTodoButton })),
            RX.createElement(reactxp_virtuallistview_1.VirtualListView, { itemList: this.state.filteredTodoList, renderItem: this._renderItem })));
    };
    TodoListPanel.prototype._filterTodoList = function (sortedTodos, searchString) {
        var lowerSearchString = searchString.toLowerCase();
        return _.filter(sortedTodos, function (item) {
            var todoLower = item.todo.text.toLowerCase();
            return todoLower.search(lowerSearchString) >= 0;
        });
    };
    return TodoListPanel;
}(resub_1.ComponentBase));
exports.default = TodoListPanel;

//# sourceMappingURL=TodoListPanel.js.map
