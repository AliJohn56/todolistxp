"use strict";
/*
* TopBarComposite.tsx
* Copyright: Microsoft 2018
*
* Horizontal bar that appears on the top of every view within the app
* when it's using composite layout (as opposed to stack-based layout).
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var images_1 = require("modules/images");
var RX = require("reactxp");
var resub_1 = require("resub");
var AccountMenuButton_1 = require("./AccountMenuButton");
var HoverButton_1 = require("../controls/HoverButton");
var NavContextStore_1 = require("../stores/NavContextStore");
var Styles_1 = require("../app/Styles");
var VerticalSeparator_1 = require("../controls/VerticalSeparator");
var _styles = {
    background: RX.Styles.createViewStyle({
        alignSelf: 'stretch',
        height: 50,
        borderBottomWidth: 1,
        borderColor: "#666" /* gray66 */,
        flexDirection: 'row',
        paddingHorizontal: 16
    }),
    logoContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'center'
    }),
    barControlsContainer: RX.Styles.createViewStyle({
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    }),
    logoImage: RX.Styles.createImageStyle({
        height: 24,
        width: 26
    }),
    logoText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displaySemibold,
        fontSize: 20 /* size20 */,
        marginHorizontal: 4,
        color: "#339933" /* logoColor */
    }),
    linkText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* menuItem */,
        marginHorizontal: 8,
        color: "#666" /* menuText */
    }),
    linkTextHover: RX.Styles.createTextStyle({
        color: "#000" /* menuTextHover */
    }),
    backButtonContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'center'
    }),
    backText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        color: "#666" /* menuText */
    })
};
var TopBarComposite = /** @class */ (function (_super) {
    tslib_1.__extends(TopBarComposite, _super);
    function TopBarComposite() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onPressBack = function (e) {
            e.stopPropagation();
            if (_this.props.onBack) {
                _this.props.onBack();
            }
        };
        _this._renderBackButton = function (isHovering) {
            return (RX.createElement(RX.View, { style: _styles.backButtonContainer },
                RX.createElement(RX.Text, { style: [_styles.backText, isHovering ? _styles.linkTextHover : undefined] }, 'Back')));
        };
        _this._onPressLogo = function (e) {
            e.stopPropagation();
            NavContextStore_1.default.navigateToTodoList('', false);
        };
        _this._onPressHelp = function (e) {
            e.stopPropagation();
            RX.Linking.openUrl('https://www.bing.com/search?q=help');
        };
        _this._onRenderHelpButton = function (isHovering) {
            var textStyles = [_styles.linkText];
            if (isHovering) {
                textStyles.push(_styles.linkTextHover);
            }
            return (RX.createElement(RX.Text, { style: textStyles }, 'Help'));
        };
        return _this;
    }
    TopBarComposite.prototype.render = function () {
        var leftContents;
        if (this.props.showBackButton) {
            leftContents = (RX.createElement(HoverButton_1.default, { onPress: this._onPressBack, onRenderChild: this._renderBackButton }));
        }
        else {
            leftContents = (RX.createElement(RX.Button, { onPress: this._onPressLogo },
                RX.createElement(RX.View, { style: _styles.logoContainer },
                    RX.createElement(RX.Image, { source: images_1.default.todoLogo, style: _styles.logoImage }),
                    RX.createElement(RX.Text, { style: _styles.logoText }, 'Todo List'))));
        }
        return (RX.createElement(RX.View, { style: _styles.background },
            leftContents,
            RX.createElement(RX.View, { style: _styles.barControlsContainer },
                RX.createElement(VerticalSeparator_1.default, null),
                RX.createElement(HoverButton_1.default, { onPress: this._onPressHelp, onRenderChild: this._onRenderHelpButton }),
                RX.createElement(VerticalSeparator_1.default, null),
                RX.createElement(AccountMenuButton_1.default, null))));
    };
    return TopBarComposite;
}(resub_1.ComponentBase));
exports.default = TopBarComposite;

//# sourceMappingURL=TopBarComposite.js.map
