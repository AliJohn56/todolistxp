"use strict";
/*
* TopBarStack.tsx
* Copyright: Microsoft 2018
*
* Horizontal bar that appears on the top of every view within the app
* when it's using stack-based layout.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var resub_1 = require("resub");
var HoverButton_1 = require("../controls/HoverButton");
var Styles_1 = require("../app/Styles");
var _styles = {
    background: RX.Styles.createViewStyle({
        alignSelf: 'stretch',
        height: 36,
        borderBottomWidth: 1,
        borderColor: "#666" /* gray66 */,
        flexDirection: 'row',
        justifyContent: 'center'
    }),
    leftRightContainer: RX.Styles.createViewStyle({
        flexDirection: 'row',
        alignItems: 'center',
        width: 60
    }),
    titleContainer: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center'
    }),
    titleText: RX.Styles.createTextStyle({
        flex: -1,
        font: Styles_1.Fonts.displaySemibold,
        fontSize: 16 /* size16 */,
        color: "#666" /* menuText */,
        textAlign: 'center'
    }),
    backText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        color: "#666" /* menuText */,
        margin: 8
    }),
    backTextHover: RX.Styles.createTextStyle({
        color: "#000" /* menuTextHover */
    })
};
var TopBarStack = /** @class */ (function (_super) {
    tslib_1.__extends(TopBarStack, _super);
    function TopBarStack() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onPressBack = function (e) {
            e.stopPropagation();
            if (_this.props.onBack) {
                _this.props.onBack();
            }
        };
        _this._renderBackButton = function (isHovering) {
            return (RX.createElement(RX.Text, { style: [_styles.backText, isHovering ? _styles.backTextHover : undefined] }, 'Back'));
        };
        return _this;
    }
    TopBarStack.prototype.render = function () {
        var leftContents;
        var rightContents;
        if (this.props.showBackButton) {
            leftContents = (RX.createElement(HoverButton_1.default, { onPress: this._onPressBack, onRenderChild: this._renderBackButton }));
        }
        return (RX.createElement(RX.View, { style: [_styles.background, Styles_1.Styles.statusBarTopMargin] },
            RX.createElement(RX.View, { style: _styles.leftRightContainer }, leftContents),
            RX.createElement(RX.View, { style: _styles.titleContainer },
                RX.createElement(RX.Text, { style: _styles.titleText, numberOfLines: 1 }, this.props.title)),
            RX.createElement(RX.View, { style: _styles.leftRightContainer }, rightContents)));
    };
    return TopBarStack;
}(resub_1.ComponentBase));
exports.default = TopBarStack;

//# sourceMappingURL=TopBarStack.js.map
