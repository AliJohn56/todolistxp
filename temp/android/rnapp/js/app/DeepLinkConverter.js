"use strict";
/*
* DeepLinkConverter.tsx
* Copyright: Microsoft 2018
*
* Converts between app (deep-link) URLs and navigation contexts.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var _ = require("lodash");
var AppConfig_1 = require("./AppConfig");
var NavActions_1 = require("../app/NavActions");
var NavModels = require("../models/NavModels");
var DeepLinkConverter = /** @class */ (function () {
    function DeepLinkConverter() {
    }
    DeepLinkConverter.getUrlFromContext = function (context) {
        var url = AppConfig_1.default.getFrontendBaseUrl();
        if (context.isStackNav) {
            var stackContext = context;
            var topViewContext = stackContext.stack[stackContext.stack.length - 1];
            if (topViewContext instanceof NavModels.TodoListViewNavContext) {
                url += '/todos';
                return url;
            }
            else if (topViewContext instanceof NavModels.ViewTodoViewNavContext) {
                url += '/todos?selected=' + encodeURIComponent(topViewContext.todoId);
                return url;
            }
            else if (topViewContext instanceof NavModels.NewTodoViewNavContext) {
                url += '/todos?selected=new';
                return url;
            }
        }
        else {
            var compositeContext = context;
            if (compositeContext instanceof NavModels.TodoRootNavContext) {
                url += '/todos';
                var todoListContext = context;
                if (todoListContext.showNewTodoPanel) {
                    url += '?selected=new';
                }
                else if (todoListContext.todoList.selectedTodoId) {
                    url += '?selected=' + encodeURIComponent(todoListContext.todoList.selectedTodoId);
                }
                return url;
            }
            else {
                // TODO - need to implement
                assert.fail('Unimplemented');
            }
        }
        return '';
    };
    DeepLinkConverter.getContextFromUrl = function (url, isStackNav) {
        var urlObj = new URL(url);
        if (!urlObj) {
            return undefined;
        }
        var pathElements = _.map(_.split(urlObj.pathname, '/'), function (elem) { return decodeURIComponent(elem); });
        if (pathElements.length < 2) {
            return undefined;
        }
        switch (pathElements[1]) {
            case 'todos':
                var selectedTodoId = void 0;
                var showNewPanel = false;
                var selectedValue = urlObj.searchParams.get('selected');
                if (selectedValue === 'new') {
                    showNewPanel = true;
                }
                else if (selectedValue) {
                    selectedTodoId = selectedValue;
                }
                return NavActions_1.default.createTodoListContext(isStackNav, selectedTodoId, showNewPanel);
            default:
                return undefined;
        }
    };
    return DeepLinkConverter;
}());
exports.default = DeepLinkConverter;

//# sourceMappingURL=DeepLinkConverter.js.map
