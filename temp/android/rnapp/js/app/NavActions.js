"use strict";
/*
* NavActions.tsx
* Copyright: Microsoft 2018
*
* Constructs navigation contexts.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var NavModels = require("../models/NavModels");
var NavActions = /** @class */ (function () {
    function NavActions() {
    }
    NavActions.createTodoListContext = function (useStackNav, selectedTodoId, showNewTodoPanel) {
        if (showNewTodoPanel === void 0) { showNewTodoPanel = false; }
        if (useStackNav) {
            var navContext = new NavModels.StackRootNavContext();
            navContext.stack.push(new NavModels.TodoListViewNavContext(selectedTodoId));
            if (showNewTodoPanel) {
                navContext.stack.push(new NavModels.NewTodoViewNavContext());
            }
            else if (selectedTodoId !== undefined) {
                navContext.stack.push(new NavModels.ViewTodoViewNavContext(selectedTodoId));
            }
            return navContext;
        }
        else {
            return new NavModels.TodoRootNavContext(selectedTodoId, showNewTodoPanel);
        }
    };
    return NavActions;
}());
exports.default = NavActions;

//# sourceMappingURL=NavActions.js.map
