"use strict";
/*
* HoverButton.tsx
* Copyright: Microsoft 2018
*
* A button that renders differently when the mouse pointer
* is hovering over it.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var HoverButton = /** @class */ (function (_super) {
    tslib_1.__extends(HoverButton, _super);
    function HoverButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onPress = function (e) {
            if (_this.props.onPress) {
                _this.props.onPress(e);
            }
        };
        _this._onHoverStart = function () {
            if (!_this.props.disabled) {
                _this.setState({ isHovering: true });
            }
        };
        _this._onHoverEnd = function () {
            if (!_this.props.disabled) {
                _this.setState({ isHovering: false });
            }
        };
        return _this;
    }
    HoverButton.prototype.render = function () {
        return (RX.createElement(RX.Button, { onPress: this._onPress, onHoverStart: this._onHoverStart, onHoverEnd: this._onHoverEnd, title: this.props.title, disabled: this.props.disabled }, this.props.onRenderChild(this.state ? this.state.isHovering : false)));
    };
    return HoverButton;
}(RX.Component));
exports.default = HoverButton;

//# sourceMappingURL=HoverButton.js.map
