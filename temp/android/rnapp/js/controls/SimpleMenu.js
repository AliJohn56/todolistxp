"use strict";
/**
* SimpleMenu.ts
* Copyright: Microsoft 2018
*
* A simple menu with text items. This component is typically
* rendered within an RX.Popup.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var _ = require("lodash");
var RX = require("reactxp");
var resub_1 = require("resub");
var KeyCodes_1 = require("../utilities/KeyCodes");
var Styles_1 = require("../app/Styles");
var _styles = {
    menuContainer: RX.Styles.createViewStyle({
        backgroundColor: "#fff" /* menuBackground */,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: "#bbb" /* menuBorder */
    }),
    menuItemContainer: RX.Styles.createButtonStyle({
        minHeight: 30,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row'
    }),
    menuItemHover: RX.Styles.createButtonStyle({
        backgroundColor: "#eee" /* menuItemHover */
    }),
    menuItemText: RX.Styles.createTextStyle({
        flex: 1,
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        color: "#666" /* menuText */,
        marginLeft: 16,
        marginRight: 32,
        marginVertical: 4
    }),
    checkMarkText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* size16 */,
        marginRight: 16,
        color: "#666" /* menuText */
    }),
    disabledText: RX.Styles.createTextStyle({
        color: "#bbb" /* menuTextDisabled */
    }),
    divider: RX.Styles.createViewStyle({
        height: 1,
        marginVertical: 4,
        backgroundColor: "#eee" /* grayEE */
    })
};
var _menuItemPrefix = 'menuitem';
var SimpleMenu = /** @class */ (function (_super) {
    tslib_1.__extends(SimpleMenu, _super);
    function SimpleMenu() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._mountedRefsMap = {};
        _this._onKeyPress = function (event) {
            var currentFocusedIndex = _this.state.focusedIndex;
            if (event.keyCode === KeyCodes_1.default.UpArrow || event.keyCode === KeyCodes_1.default.LeftArrow) {
                currentFocusedIndex = currentFocusedIndex - 1;
            }
            if (event.keyCode === KeyCodes_1.default.DownArrow || event.keyCode === KeyCodes_1.default.RightArrow) {
                currentFocusedIndex = currentFocusedIndex + 1;
            }
            if (currentFocusedIndex < 0 || currentFocusedIndex >= _this.props.menuItems.length) {
                return;
            }
            _this.setState({ focusedIndex: currentFocusedIndex });
        };
        return _this;
    }
    SimpleMenu.prototype._buildState = function (props, initialBuild) {
        if (initialBuild) {
            return {
                hoverCommand: undefined,
                focusedIndex: -1
            };
        }
        return {};
    };
    SimpleMenu.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        if (this.props.focusFirst) {
            this.focusFirt();
        }
    };
    SimpleMenu.prototype.componentDidUpdate = function (prevProps, prevState, prevContext) {
        _super.prototype.componentDidUpdate.call(this, prevProps, prevState, prevContext);
        if (this.state.focusedIndex !== prevState.focusedIndex) {
            this.focusItem(this.state.focusedIndex);
        }
    };
    SimpleMenu.prototype.focusFirt = function () {
        this.focusItem(0);
    };
    SimpleMenu.prototype.focusLast = function () {
        this.focusItem(this.props.menuItems.length - 1);
    };
    SimpleMenu.prototype.focusItem = function (index) {
        if (index === void 0) { index = 0; }
        if (index < 0 || index > this.props.menuItems.length - 1) {
            return;
        }
        if (this.state.focusedIndex === index) {
            var ref = this._mountedRefsMap.ref(_menuItemPrefix + index);
            if (ref) {
                ref.requestFocus();
            }
        }
        else {
            this.setState({ focusedIndex: index });
        }
    };
    SimpleMenu.prototype.render = function () {
        var _this = this;
        var menuItems = _.map(this.props.menuItems, function (item, index) {
            var buttonContainerStyles = [_styles.menuItemContainer];
            if (item.text === '-') {
                return (RX.createElement(RX.View, { key: 'div-' + index, style: _styles.divider }));
            }
            if (_this.props.menuButtonStyles) {
                buttonContainerStyles.push(_this.props.menuButtonStyles);
            }
            var textStyles = [_styles.menuItemText];
            if (_this.props.menuTextStyles) {
                textStyles.push(_this.props.menuTextStyles);
            }
            if (item.command === _this.state.hoverCommand && !item.disabled) {
                buttonContainerStyles.push(_styles.menuItemHover);
            }
            var accessibilityLabel = item.text;
            var selectedCheckMark;
            if (item.checked) {
                selectedCheckMark = (RX.createElement(RX.Text, { style: _styles.checkMarkText }, ''));
            }
            else if (item.disabled) {
                textStyles.push(_styles.disabledText);
            }
            return (RX.createElement(RX.Button, { ref: function (elem) { return _this._mountedRefsMap[_menuItemPrefix + index] = elem; }, key: item.command, style: buttonContainerStyles, onPress: function (e) { return _this._onClickItem(e, item); }, onHoverStart: function () { return _this._onMouseEnter(item); }, onHoverEnd: function () { return _this._onMouseLeave(item); }, disabled: item.disabled, tabIndex: index === _this.state.focusedIndex ? 0 : -1, accessibilityTraits: RX.Types.AccessibilityTrait.MenuItem, accessibilityLabel: accessibilityLabel },
                RX.createElement(RX.Text, { style: textStyles }, item.text),
                selectedCheckMark));
        });
        return (RX.createElement(RX.View, { style: _styles.menuContainer, onKeyPress: this._onKeyPress }, menuItems));
    };
    SimpleMenu.prototype._onClickItem = function (e, item) {
        e.stopPropagation();
        if (!item.disabled && this.props.onSelectItem) {
            this.props.onSelectItem(item.command);
        }
    };
    SimpleMenu.prototype._onMouseEnter = function (item) {
        if (!item.disabled && item.command !== this.state.hoverCommand) {
            this.setState({
                hoverCommand: item.command
            });
        }
    };
    SimpleMenu.prototype._onMouseLeave = function (item) {
        if (!item.disabled && item.command === this.state.hoverCommand) {
            this.setState({
                hoverCommand: undefined
            });
        }
    };
    return SimpleMenu;
}(resub_1.ComponentBase));
exports.default = SimpleMenu;

//# sourceMappingURL=SimpleMenu.js.map
