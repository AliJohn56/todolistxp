"use strict";
/*
* VerticalSeparator.tsx
* Copyright: Microsoft 2018
*
* A simple vertical line that separates items.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var _styles = {
    separator: RX.Styles.createViewStyle({
        alignSelf: 'stretch',
        marginVertical: 12,
        marginHorizontal: 8,
        width: 1,
        backgroundColor: "#eee" /* separator */
    })
};
var VerticalSeparator = /** @class */ (function (_super) {
    tslib_1.__extends(VerticalSeparator, _super);
    function VerticalSeparator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VerticalSeparator.prototype.render = function () {
        return (RX.createElement(RX.View, { style: [_styles.separator, this.props.style] }));
    };
    return VerticalSeparator;
}(RX.Component));
exports.default = VerticalSeparator;

//# sourceMappingURL=VerticalSeparator.js.map
