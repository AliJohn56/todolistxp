"use strict";
/**
* index.web.ts
* Copyright: Microsoft 2018
*
* Web implementation of "images" module.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var AppConfig_1 = require("../../app/AppConfig");
var ImageSource = /** @class */ (function () {
    function ImageSource() {
        this.todoLogo = AppConfig_1.default.getImagePath('todo-logo.png');
        this.todoSmall = AppConfig_1.default.getImagePath('todo-small.png');
    }
    return ImageSource;
}());
exports.default = new ImageSource();

//# sourceMappingURL=index.web.js.map
