"use strict";
/**
 * ExceptionReporter.ts
 * Copyright: Microsoft 2018
 *
 * Class that hooks the uncaught exception handler and reports exceptions to the
 * user via message boxes. This should be used only in debug mode.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var ExceptionReporter = /** @class */ (function () {
    function ExceptionReporter() {
        var _this = this;
        this._handlers = [];
        window.onerror = function (event, source, fileNum, columnNum) {
            var extData = [];
            for (var _i = 4; _i < arguments.length; _i++) {
                extData[_i - 4] = arguments[_i];
            }
            // Modern Browsers will support this
            var stack = '';
            var name = '';
            if (extData && extData[0]) {
                stack = extData[0]['stack'] || stack;
                name = extData[0]['name'] || name;
            }
            var swallowError = false;
            _this._handlers.forEach(function (handler) {
                try {
                    if (handler !== null) {
                        swallowError = swallowError || handler(event.toString(), source, fileNum, columnNum, name, stack);
                    }
                }
                catch (err) {
                    assert.fail('Error handling Exception: ' + JSON.stringify(err));
                }
            });
            return swallowError;
        };
    }
    ExceptionReporter.prototype.register = function (handler) {
        if (handler !== null) {
            this._handlers.push(handler);
        }
    };
    ExceptionReporter.prototype.registerAlertView = function () {
        this._handlers.push(function (event, source, fileno, columnNumber) {
            window.alert('DEBUG ALERT: Uncaught Exception\n' + event + '\n' + source + ' (' + fileno + ',' + columnNumber + ')');
            return false;
        });
    };
    ExceptionReporter.prototype.registerConsoleView = function () {
        this._handlers.push(function (event, source, fileno, columnNumber, errName, stack) {
            console.error('DEBUG ALERT: Uncaught Exception\n' + event + '\n' + source +
                ' (' + fileno + ',' + columnNumber + ')\nStack:\n' + stack);
            return false;
        });
    };
    ExceptionReporter.prototype.unregister = function () {
        this._handlers = [];
    };
    return ExceptionReporter;
}());
exports.default = ExceptionReporter;

//# sourceMappingURL=ExceptionReporter.js.map
