"use strict";
/**
* KeysCodes.ts
* Copyright: Microsoft 2018
*
* Key codes for key press events.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var RX = require("reactxp");
var _isInitialized = false;
var _isReactNative = false;
function isReactNative() {
    if (!_isInitialized) {
        var platform = RX.Platform.getType();
        _isReactNative = platform !== 'web';
    }
    return _isReactNative;
}
var Keys;
(function (Keys) {
    // ROMAN ALPHA
    Keys[Keys["A"] = 65] = "A";
    Keys[Keys["B"] = 66] = "B";
    Keys[Keys["C"] = 67] = "C";
    Keys[Keys["D"] = 68] = "D";
    Keys[Keys["E"] = 69] = "E";
    Keys[Keys["F"] = 70] = "F";
    Keys[Keys["G"] = 71] = "G";
    Keys[Keys["H"] = 72] = "H";
    Keys[Keys["I"] = 73] = "I";
    Keys[Keys["J"] = 74] = "J";
    Keys[Keys["K"] = 75] = "K";
    Keys[Keys["L"] = 76] = "L";
    Keys[Keys["M"] = 77] = "M";
    Keys[Keys["N"] = 78] = "N";
    Keys[Keys["O"] = 79] = "O";
    Keys[Keys["P"] = 80] = "P";
    Keys[Keys["Q"] = 81] = "Q";
    Keys[Keys["R"] = 82] = "R";
    Keys[Keys["S"] = 83] = "S";
    Keys[Keys["T"] = 84] = "T";
    Keys[Keys["U"] = 85] = "U";
    Keys[Keys["V"] = 86] = "V";
    Keys[Keys["W"] = 87] = "W";
    Keys[Keys["X"] = 88] = "X";
    Keys[Keys["Y"] = 89] = "Y";
    Keys[Keys["Z"] = 90] = "Z";
    // ARROW KEYS
    Keys[Keys["LeftArrow"] = isReactNative() ? 21 : 37] = "LeftArrow";
    Keys[Keys["UpArrow"] = isReactNative() ? 19 : 38] = "UpArrow";
    Keys[Keys["RightArrow"] = isReactNative() ? 22 : 39] = "RightArrow";
    Keys[Keys["DownArrow"] = isReactNative() ? 20 : 40] = "DownArrow";
    // NUMERALS
    Keys[Keys["Zero"] = 48] = "Zero";
    Keys[Keys["One"] = 49] = "One";
    Keys[Keys["Two"] = 50] = "Two";
    Keys[Keys["Three"] = 51] = "Three";
    Keys[Keys["Four"] = 52] = "Four";
    Keys[Keys["Five"] = 53] = "Five";
    Keys[Keys["Six"] = 54] = "Six";
    Keys[Keys["Seven"] = 55] = "Seven";
    Keys[Keys["Eight"] = 56] = "Eight";
    Keys[Keys["Nine"] = 57] = "Nine";
    // OTHER
    Keys[Keys["Tab"] = 9] = "Tab";
    Keys[Keys["Shift"] = 16] = "Shift";
    Keys[Keys["Escape"] = 27] = "Escape";
    Keys[Keys["Return"] = 13] = "Return";
    Keys[Keys["Enter"] = 13] = "Enter";
    Keys[Keys["Alt"] = 18] = "Alt";
    Keys[Keys["Option"] = 18] = "Option";
    Keys[Keys["Command"] = 224] = "Command";
    Keys[Keys["Control"] = 17] = "Control";
    Keys[Keys["Delete"] = 8] = "Delete";
    Keys[Keys["Space"] = 32] = "Space";
    Keys[Keys["PageUp"] = isReactNative() ? 92 : 33] = "PageUp";
    Keys[Keys["PageDown"] = isReactNative() ? 93 : 34] = "PageDown";
    Keys[Keys["Insert"] = 45] = "Insert";
    Keys[Keys["Comma"] = 188] = "Comma";
})(Keys || (Keys = {}));
exports.default = Keys;

//# sourceMappingURL=KeyCodes.js.map
