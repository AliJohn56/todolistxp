"use strict";
/*
* AccountMenuButton.tsx
* Copyright: Microsoft 2018
*
* Button that displays the currently-signed-in user and provides
* a popup menu that allows the user to sign out, adjust account
* settings, etc.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var resub_1 = require("resub");
var CurrentUserStore_1 = require("../stores/CurrentUserStore");
var SimpleMenu_1 = require("../controls/SimpleMenu");
var Styles_1 = require("../app/Styles");
var _menuPopupId = 'accountMenu';
var _styles = {
    buttonContainer: RX.Styles.createButtonStyle({
        paddingHorizontal: 4,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems: 'center'
    }),
    nameText: RX.Styles.createTextStyle({
        font: Styles_1.Fonts.displayRegular,
        fontSize: 16 /* menuItem */,
        marginHorizontal: 8,
        color: "#666" /* menuText */
    }),
    nameTextHover: RX.Styles.createTextStyle({
        color: "#000" /* menuTextHover */
    }),
    circleGlyph: RX.Styles.createViewStyle({
        width: 12,
        height: 12,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: "#666" /* menuText */,
        backgroundColor: "#339933" /* logoColor */
    }),
    circleGlyphHover: RX.Styles.createViewStyle({
        borderColor: "#000" /* menuTextHover */
    })
};
var AccountMenuButton = /** @class */ (function (_super) {
    tslib_1.__extends(AccountMenuButton, _super);
    function AccountMenuButton() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onMountButton = function (elem) {
            _this._mountedButton = elem;
        };
        _this._onPress = function (e) {
            e.stopPropagation();
            RX.Popup.show({
                getAnchor: function () {
                    return _this._mountedButton;
                },
                getElementTriggeringPopup: function () {
                    return _this._mountedButton;
                },
                renderPopup: function (anchorPosition, anchorOffset, popupWidth, popupHeight) {
                    var items = [{
                            command: 'settings',
                            text: 'Account Settings'
                        }, {
                            command: '',
                            text: '-'
                        }, {
                            command: 'signout',
                            text: 'Sign Out'
                        }];
                    return (RX.createElement(SimpleMenu_1.default, { menuItems: items, onSelectItem: _this._onSelectMenuItem }));
                },
                dismissIfShown: true
            }, _menuPopupId);
        };
        _this._onSelectMenuItem = function (command) {
            RX.Popup.dismiss(_menuPopupId);
            // TODO - need to implement
        };
        return _this;
    }
    AccountMenuButton.prototype._buildState = function (props, initState) {
        var partialState = {
            currentUserName: CurrentUserStore_1.default.getFullName()
        };
        return partialState;
    };
    AccountMenuButton.prototype.render = function () {
        var _this = this;
        return (RX.createElement(RX.Button, { ref: this._onMountButton, style: _styles.buttonContainer, onPress: this._onPress, onHoverStart: function () { return _this.setState({ isHovering: true }); }, onHoverEnd: function () { return _this.setState({ isHovering: false }); } },
            RX.createElement(RX.View, { style: [_styles.circleGlyph, this.state.isHovering ? _styles.circleGlyphHover : undefined] }),
            RX.createElement(RX.Text, { style: [_styles.nameText, this.state.isHovering ? _styles.nameTextHover : undefined] }, this.state.currentUserName)));
    };
    return AccountMenuButton;
}(resub_1.ComponentBase));
exports.default = AccountMenuButton;

//# sourceMappingURL=AccountMenuButton.js.map
