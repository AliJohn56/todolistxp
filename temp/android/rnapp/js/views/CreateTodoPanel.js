"use strict";
/**
* CreateTodoPanel.tsx
* Copyright: Microsoft 2017
*
* The Todo item edit view.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var NavContextStore_1 = require("../stores/NavContextStore");
var SimpleButton_1 = require("../controls/SimpleButton");
var Styles_1 = require("../app/Styles");
var TodosStore_1 = require("../stores/TodosStore");
var _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        padding: 16
    }),
    editTodoItem: RX.Styles.createTextInputStyle({
        margin: 8,
        height: 32,
        paddingHorizontal: 4,
        fontSize: 16 /* size16 */,
        alignSelf: 'stretch'
    }),
    buttonContainer: RX.Styles.createViewStyle({
        margin: 8,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    })
};
var CreateTodoPanel = /** @class */ (function (_super) {
    tslib_1.__extends(CreateTodoPanel, _super);
    function CreateTodoPanel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onChangeText = function (newText) {
            _this.setState({ todoText: newText });
        };
        _this._onSubmitText = function () {
            _this._saveTodo();
        };
        _this._onPressSave = function () {
            _this._saveTodo();
        };
        return _this;
    }
    CreateTodoPanel.prototype.render = function () {
        return (RX.createElement(RX.View, { useSafeInsets: true, style: [_styles.container, Styles_1.Styles.statusBarTopMargin] },
            RX.createElement(RX.TextInput, { style: _styles.editTodoItem, value: this.state ? this.state.todoText : '', placeholder: 'Enter todo', onChangeText: this._onChangeText, onSubmitEditing: this._onSubmitText, accessibilityId: 'EditTodoPanelTextInput' }),
            RX.createElement(RX.View, { style: _styles.buttonContainer },
                RX.createElement(SimpleButton_1.default, { text: 'Save', onPress: this._onPressSave, disabled: !!this.state && !this.state.todoText }))));
    };
    CreateTodoPanel.prototype._saveTodo = function () {
        if (!!this.state && this.state.todoText) {
            var newTodo = TodosStore_1.default.addTodo(this.state.todoText);
            this.setState({ todoText: '' });
            NavContextStore_1.default.navigateToTodoList(NavContextStore_1.default.isUsingStackNav() ? undefined : newTodo.id);
        }
    };
    return CreateTodoPanel;
}(RX.Component));
exports.default = CreateTodoPanel;

//# sourceMappingURL=CreateTodoPanel.js.map
