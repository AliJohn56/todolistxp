"use strict";
/*
* TodoCompositeView.tsx
* Copyright: Microsoft 2018
*
* Main view that provides a composite view of todos on the left and
* details of the selected todo on the right.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var resub_1 = require("resub");
var CreateTodoPanel_1 = require("./CreateTodoPanel");
var NavContextStore_1 = require("../stores/NavContextStore");
var TodoListPanel_1 = require("./TodoListPanel");
var ViewTodoPanel_1 = require("./ViewTodoPanel");
var _styles = {
    viewContainer: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        flexDirection: 'row'
    }),
    leftPanelContainer: RX.Styles.createViewStyle({
        width: 400,
        flexDirection: 'column'
    }),
    rightPanelContainer: RX.Styles.createViewStyle({
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "#f8f8f8" /* grayF8 */
    })
};
var TodoCompositeView = /** @class */ (function (_super) {
    tslib_1.__extends(TodoCompositeView, _super);
    function TodoCompositeView() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onSelectTodo = function (todoId) {
            NavContextStore_1.default.navigateToTodoList(todoId, false);
        };
        _this._onCreateNewTodo = function () {
            NavContextStore_1.default.navigateToTodoList('', true);
        };
        return _this;
    }
    TodoCompositeView.prototype._buildState = function (props, initState) {
        return undefined;
    };
    TodoCompositeView.prototype.render = function () {
        return (RX.createElement(RX.View, { style: _styles.viewContainer },
            RX.createElement(RX.View, { style: _styles.leftPanelContainer },
                RX.createElement(TodoListPanel_1.default, { selectedTodoId: this.props.navContext.todoList.selectedTodoId || '', onSelect: this._onSelectTodo, onCreateNew: this._onCreateNewTodo })),
            RX.createElement(RX.View, { style: _styles.rightPanelContainer }, this._renderRightPanel())));
    };
    TodoCompositeView.prototype._renderRightPanel = function () {
        if (this.props.navContext.showNewTodoPanel) {
            return (RX.createElement(CreateTodoPanel_1.default, null));
        }
        else if (this.props.navContext.todoList.selectedTodoId) {
            return (RX.createElement(ViewTodoPanel_1.default, { todoId: this.props.navContext.todoList.selectedTodoId }));
        }
        else {
            return null;
        }
    };
    return TodoCompositeView;
}(resub_1.ComponentBase));
exports.default = TodoCompositeView;

//# sourceMappingURL=TodoCompositeView.js.map
