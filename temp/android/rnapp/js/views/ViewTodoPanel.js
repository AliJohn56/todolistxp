"use strict";
/**
* ViewTodoPanel.tsx
* Copyright: Microsoft 2017
*
* The Todo item edit view.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var RX = require("reactxp");
var resub_1 = require("resub");
var NavContextStore_1 = require("../stores/NavContextStore");
var SimpleButton_1 = require("../controls/SimpleButton");
var SimpleDialog_1 = require("../controls/SimpleDialog");
var TodosStore_1 = require("../stores/TodosStore");
var _styles = {
    container: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        margin: 16
    }),
    todoText: RX.Styles.createTextStyle({
        margin: 8,
        fontSize: 16 /* size16 */,
        alignSelf: 'stretch',
        backgroundColor: 'transparent'
    }),
    buttonContainer: RX.Styles.createViewStyle({
        margin: 8,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    })
};
var _confirmDeleteDialogId = 'delete';
var ViewTodoPanel = /** @class */ (function (_super) {
    tslib_1.__extends(ViewTodoPanel, _super);
    function ViewTodoPanel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._onPressDelete = function (e) {
            e.stopPropagation();
            var dialog = (RX.createElement(SimpleDialog_1.default, { dialogId: _confirmDeleteDialogId, text: 'Are you sure you want to delete this todo?', buttons: [{
                        text: 'Delete',
                        onPress: function () {
                            SimpleDialog_1.default.dismissAnimated(_confirmDeleteDialogId);
                            _this._completeDelete();
                        }
                    }, {
                        text: 'Cancel',
                        isCancel: true,
                        onPress: function () {
                            SimpleDialog_1.default.dismissAnimated(_confirmDeleteDialogId);
                        }
                    }] }));
            RX.Modal.show(dialog, _confirmDeleteDialogId);
        };
        return _this;
    }
    ViewTodoPanel.prototype._buildState = function (props, initState) {
        var newState = {
            todo: TodosStore_1.default.getTodoById(props.todoId)
        };
        return newState;
    };
    ViewTodoPanel.prototype.render = function () {
        return (RX.createElement(RX.View, { useSafeInsets: true, style: _styles.container },
            RX.createElement(RX.Text, { style: _styles.todoText }, this.state.todo ? this.state.todo.text : ''),
            RX.createElement(RX.View, { style: _styles.buttonContainer },
                RX.createElement(SimpleButton_1.default, { text: 'Delete', onPress: this._onPressDelete, disabled: !this.state.todo }))));
    };
    ViewTodoPanel.prototype._completeDelete = function () {
        TodosStore_1.default.deleteTodo(this.state.todo.id);
        NavContextStore_1.default.navigateToTodoList();
    };
    return ViewTodoPanel;
}(resub_1.ComponentBase));
exports.default = ViewTodoPanel;

//# sourceMappingURL=ViewTodoPanel.js.map
