"use strict";
/**
* AppBootstrapper.tsx
* Copyright: Microsoft 2018
*
* Main entry point for the app, common to both native and web.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var RX = require("reactxp");
var AppConfig_1 = require("./AppConfig");
var DeepLinkConverter_1 = require("./DeepLinkConverter");
var LocalDb_1 = require("./LocalDb");
var NavContextStore_1 = require("../stores/NavContextStore");
var PageUrlService_1 = require("../services/PageUrlService");
var ResponsiveWidthStore_1 = require("../stores/ResponsiveWidthStore");
var RootView_1 = require("../views/RootView");
var ServiceManager_1 = require("../services/ServiceManager");
var ServiceRegistrar_1 = require("../services/ServiceRegistrar");
var TodosStore_1 = require("../stores/TodosStore");
var AppBootstrapper = /** @class */ (function () {
    function AppBootstrapper() {
        var _this = this;
        this._onLayoutRootView = function (e) {
            var width = e.width, height = e.height;
            ResponsiveWidthStore_1.default.putWindowSize(width, height);
        };
        RX.App.initialize(__DEV__, __DEV__);
        ServiceRegistrar_1.default.init();
        // Open the DB and startup any critical services before displaying the UI.
        LocalDb_1.default.open(this._getDbProvidersToTry()).then(function () {
            return _this._startCriticalServices();
        }).then(function () {
            RX.UserInterface.setMainView(_this._renderRootView());
            // Convert the initial URL into a navigation context.
            _this._getInitialUrl().then(function (url) {
                if (url) {
                    var context = DeepLinkConverter_1.default.getContextFromUrl(url, NavContextStore_1.default.isUsingStackNav());
                    if (context) {
                        NavContextStore_1.default.setNavContext(context);
                    }
                }
            });
        });
    }
    AppBootstrapper.prototype._startCriticalServices = function () {
        var servicesToStart = [TodosStore_1.default];
        if (AppConfig_1.default.getPlatformType() === 'web') {
            servicesToStart.push(PageUrlService_1.default);
        }
        return ServiceManager_1.default.ensureStarted(servicesToStart);
    };
    AppBootstrapper.prototype._renderRootView = function () {
        return (RX.createElement(RootView_1.default, { onLayout: this._onLayoutRootView }));
    };
    return AppBootstrapper;
}());
exports.default = AppBootstrapper;

//# sourceMappingURL=AppBootstrapper.js.map
