"use strict";
/**
* AppBootstrapperNative.tsx
* Copyright: Microsoft 2018
*
* Main entry point for the native app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
// Do shimming before anything else.
var ShimHelpers = require("../utilities/ShimHelpers");
ShimHelpers.shimEnvironment(__DEV__, true);
// Initialize AppConfig.
var AppConfig_1 = require("./AppConfig");
AppConfig_1.default.initialize({});
var CordovaNativeSqliteProvider_1 = require("nosqlprovider/dist/CordovaNativeSqliteProvider");
var InMemoryProvider_1 = require("nosqlprovider/dist/InMemoryProvider");
var RX = require("reactxp");
var AppBootstrapper_1 = require("./AppBootstrapper");
var AppBootstrapperNative = /** @class */ (function (_super) {
    tslib_1.__extends(AppBootstrapperNative, _super);
    function AppBootstrapperNative() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppBootstrapperNative.prototype._getDbProvidersToTry = function () {
        var rnSqliteProvider = require('react-native-sqlite-storage');
        // Specify the DB providers that are valid on the RN platforms.
        return [
            new CordovaNativeSqliteProvider_1.CordovaNativeSqliteProvider(rnSqliteProvider),
            new InMemoryProvider_1.InMemoryProvider()
        ];
    };
    AppBootstrapperNative.prototype._getInitialUrl = function () {
        return RX.Linking.getInitialUrl();
    };
    return AppBootstrapperNative;
}(AppBootstrapper_1.default));
exports.default = new AppBootstrapperNative();

//# sourceMappingURL=AppBootstrapperNative.js.map
