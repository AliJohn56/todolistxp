"use strict";
/**
* LocalDb.tsx
* Copyright: Microsoft 2018
*
* Local database implementation and interface for the app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var SyncTasks = require("synctasks");
var _appDatabaseName = 'todoDb';
var _appSchemaVersion = 1;
var Stores = {
    todoItems: 'todoItems_v1'
};
var Indexes = {
    todoSearchTerms: 'todoItems_searchTerms_v1'
};
var _appSchema = {
    version: _appSchemaVersion,
    lastUsableVersion: _appSchemaVersion,
    stores: [
        {
            name: Stores.todoItems,
            primaryKeyPath: 'id',
            indexes: [
                {
                    name: Indexes.todoSearchTerms,
                    keyPath: '_searchTerms',
                    fullText: true
                }
            ]
        }
    ]
};
var LocalDb = /** @class */ (function () {
    function LocalDb() {
        this._handleDbFail = function (err) {
            if (err.target) {
                if (err.target.error) {
                    var error = err.target.error;
                    console.error("IDBRequest: " + error.name + ": " + error.message);
                }
                if (err.target.transaction && err.target.transaction.error) {
                    var error = err.target.transaction.error;
                    console.error("IDBTransaction: " + error.name + ": " + error.message);
                }
                if (err.target.source) {
                    var source = err.target.source;
                    console.error("IDBStore: " + source.name + ", " + source.keyPath + ", indexes: " + source.indexNames.join());
                }
            }
        };
    }
    LocalDb.prototype.open = function (providersToTry) {
        var _this = this;
        return this._openListOfProviders(providersToTry, _appDatabaseName, _appSchema).then(function (prov) {
            _this._db = prov;
        });
    };
    LocalDb.prototype._openListOfProviders = function (providersToTry, dbName, schema) {
        var task = SyncTasks.Defer();
        var providerIndex = 0;
        var errorList = [];
        console.log('Opening Database: Providers: ' + providersToTry.length);
        var tryNext = function () {
            if (providerIndex >= providersToTry.length) {
                task.reject(errorList.length <= 1 ? errorList[0] : errorList);
                return;
            }
            var provider = providersToTry[providerIndex];
            provider.open(dbName, schema, false, false).then(function () {
                console.log('Provider ' + providerIndex + ': Open Success!');
                task.resolve(provider);
            }, function (err) {
                console.error('Provider ' + providerIndex + ': Open Failure: ' + JSON.stringify(err));
                errorList.push(err);
                providerIndex++;
                tryNext();
            });
        };
        tryNext();
        return task.promise();
    };
    // Returns all todo items from the DB.
    LocalDb.prototype.getAllTodos = function () {
        if (!this._db) {
            return SyncTasks.Rejected('Database not open');
        }
        return this._db.openTransaction([Stores.todoItems], false).then(function (tx) {
            return tx.getStore(Stores.todoItems);
        }).then(function (store) {
            return store.openPrimaryKey().getAll();
        }).fail(this._handleDbFail);
    };
    // Adds a new todo item to the DB.
    LocalDb.prototype.putTodo = function (todo) {
        if (!this._db) {
            return SyncTasks.Rejected('Database not open');
        }
        return this._db.openTransaction([Stores.todoItems], true).then(function (tx) {
            return tx.getStore(Stores.todoItems);
        }).then(function (store) {
            return store.put(todo);
        }).fail(this._handleDbFail);
    };
    /**
     * Deletes a todo item from the DB
     * @param todoId item to delete
     */
    LocalDb.prototype.deleteTodo = function (todoId) {
        if (!this._db) {
            return SyncTasks.Rejected('Database not open');
        }
        return this._db.openTransaction([Stores.todoItems], true).then(function (tx) {
            return tx.getStore(Stores.todoItems);
        }).then(function (store) {
            return store.remove(todoId);
        }).fail(this._handleDbFail);
    };
    return LocalDb;
}());
exports.default = new LocalDb();

//# sourceMappingURL=LocalDb.js.map
