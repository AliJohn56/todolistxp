"use strict";
/**
* Modal.ts
* Copyright: Microsoft 2018
*
* Modal dialog container, typically embedded within RX.Modal.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var assert = require("assert");
var RX = require("reactxp");
var resub_1 = require("resub");
var SyncTasks = require("synctasks");
var KeyCodes_1 = require("../utilities/KeyCodes");
var _opacityAnimationDuration = 150;
var _scalingAnimationDuration = 250;
var _initialScalingRatio = 0.95;
var _styles = {
    modalContainerBackground: RX.Styles.createViewStyle({
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0, 0, 0, 0.05)" /* simpleDialogBehind */,
        flexDirection: 'row'
    }),
    modalContainer: RX.Styles.createViewStyle({
        flex: -1,
        flexDirection: 'row'
    }),
    modalBox: RX.Styles.createViewStyle({
        flex: -1,
        margin: 32
    })
};
var Modal = /** @class */ (function (_super) {
    tslib_1.__extends(Modal, _super);
    function Modal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._contentOpacityValue = new RX.Animated.Value(1);
        _this._contentScaleValue = new RX.Animated.Value(_initialScalingRatio);
        _this._contentScaleAnimationStyle = RX.Styles.createAnimatedViewStyle({
            opacity: _this._contentOpacityValue,
            transform: [{
                    scale: _this._contentScaleValue
                }]
        });
        _this._opacityAnimationValue = new RX.Animated.Value(1);
        _this._opacityAnimationStyle = RX.Styles.createAnimatedViewStyle({
            opacity: _this._opacityAnimationValue
        });
        _this._onKeyUp = function (e) {
            if (e.keyCode === KeyCodes_1.default.Escape) {
                _this._clickOutside(e);
                return true;
            }
            return false;
        };
        _this._clickInside = function (e) {
            // Do nothing, keeps click/press from propogating up to the dismissal action.
            e.stopPropagation();
        };
        _this._onLongPressOutside = function (e) {
            // Do nothing, required to keep onPress from firing on long press.
            e.stopPropagation();
        };
        _this._clickOutside = function (e) {
            e.stopPropagation();
        };
        return _this;
    }
    Modal.prototype._buildState = function (props, initialBuild) {
        var newState = {};
        newState.widthStyle = props.modalWidth ? RX.Styles.createViewStyle({
            width: props.modalWidth
        }, false) : undefined;
        newState.heightStyle = props.modalHeight ? RX.Styles.createViewStyle({
            height: props.modalHeight
        }, false) : undefined;
        return newState;
    };
    Modal.prototype.componentWillMount = function () {
        // To give children a chance to cancel the ESC handler,
        // subscribing in componentWillMount so that the children
        // could subscribe after.
        _super.prototype.componentWillMount.call(this);
        RX.Input.keyUpEvent.subscribe(this._onKeyUp);
    };
    Modal.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        Modal._visibleModalMap[this.props.modalId] = this;
        RX.Animated.timing(this._contentScaleValue, {
            toValue: 1,
            duration: _scalingAnimationDuration,
            easing: RX.Animated.Easing.OutBack(),
            useNativeDriver: true
        }).start();
    };
    Modal.prototype.componentWillUnmount = function () {
        _super.prototype.componentWillUnmount.call(this);
        delete Modal._visibleModalMap[this.props.modalId];
        RX.Input.keyUpEvent.unsubscribe(this._onKeyUp);
    };
    Modal.prototype.componentWillUpdate = function (newProps, newState, newContext) {
        _super.prototype.componentWillUpdate.call(this, newProps, newState, newContext);
        // We assume the modalId doesn't change.
        assert.ok(newProps.modalId === this.props.modalId);
    };
    Modal.prototype.render = function () {
        var modalBoxStyles = [_styles.modalBox, this.state.widthStyle];
        var modalContentStyles = [_styles.modalContainer, this._contentScaleAnimationStyle, this.state.heightStyle];
        var modalContent = (RX.createElement(RX.Animated.View, { style: modalContentStyles },
            RX.createElement(RX.View, { style: modalBoxStyles, onPress: this._clickInside, accessibilityTraits: RX.Types.AccessibilityTrait.Dialog, restrictFocusWithin: true, disableTouchOpacityAnimation: true, tabIndex: -1 }, this.props.children)));
        return (RX.createElement(RX.Animated.View, { style: [_styles.modalContainerBackground, this._opacityAnimationStyle], onPress: this._clickOutside, onLongPress: this._onLongPressOutside, disableTouchOpacityAnimation: true }, modalContent));
    };
    Modal.prototype._animateClose = function (onAnimationComplete) {
        RX.Animated.parallel([
            RX.Animated.timing(this._opacityAnimationValue, {
                toValue: 0,
                duration: _opacityAnimationDuration,
                easing: RX.Animated.Easing.Out(),
                useNativeDriver: true
            }),
            RX.Animated.timing(this._contentOpacityValue, {
                toValue: 0,
                duration: _opacityAnimationDuration,
                easing: RX.Animated.Easing.Out(),
                useNativeDriver: true
            }),
            RX.Animated.timing(this._contentScaleValue, {
                toValue: _initialScalingRatio,
                duration: _scalingAnimationDuration,
                easing: RX.Animated.Easing.Out(),
                useNativeDriver: true
            })
        ]).start(function () {
            onAnimationComplete();
        });
    };
    Modal.dismissAnimated = function (modalId) {
        var modal = Modal._visibleModalMap[modalId];
        if (!modal) {
            return SyncTasks.Rejected('Modal ID not found');
        }
        var deferred = SyncTasks.Defer();
        modal._animateClose(function () {
            RX.Modal.dismiss(modalId);
            deferred.resolve(void 0);
        });
        return deferred.promise();
    };
    Modal._visibleModalMap = {};
    return Modal;
}(resub_1.ComponentBase));
exports.default = Modal;

//# sourceMappingURL=Modal.js.map
