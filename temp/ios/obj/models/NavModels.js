"use strict";
/**
* NavModels.ts
* Copyright: Microsoft 2018
*
* Interface and enumeration definitions used for app navigation.
* A "navigation context" describes a location within the app and
* any associated state that may be expressed within a deep link.
*
* A "view nav context" describes the state of a view.
*
* A "root nav context" describes the nav context for the app's
* root view - the top of the visual hierarchy. Depending on the
* screen size, the root nav context may be stack-based (consisting
* of a stack of individual panels) or composite (in which multiple
* views are displayed side by side).
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var _ = require("lodash");
var NavViewId;
(function (NavViewId) {
    NavViewId[NavViewId["TodoComposite"] = 1] = "TodoComposite";
    NavViewId[NavViewId["TodoList"] = 2] = "TodoList";
    NavViewId[NavViewId["NewTodo"] = 3] = "NewTodo";
    NavViewId[NavViewId["ViewTodo"] = 4] = "ViewTodo";
})(NavViewId = exports.NavViewId || (exports.NavViewId = {}));
//----------------------------------------
// Root nav contexts
//----------------------------------------
var RootNavContext = /** @class */ (function () {
    function RootNavContext(isStackNav) {
        this.isStackNav = isStackNav;
    }
    return RootNavContext;
}());
exports.RootNavContext = RootNavContext;
var CompositeRootNavContext = /** @class */ (function (_super) {
    tslib_1.__extends(CompositeRootNavContext, _super);
    function CompositeRootNavContext(viewId) {
        var _this = _super.call(this, false) || this;
        _this.viewId = viewId;
        return _this;
    }
    return CompositeRootNavContext;
}(RootNavContext));
exports.CompositeRootNavContext = CompositeRootNavContext;
var StackRootNavContext = /** @class */ (function (_super) {
    tslib_1.__extends(StackRootNavContext, _super);
    function StackRootNavContext() {
        var _this = _super.call(this, true) || this;
        _this.stack = [];
        return _this;
    }
    StackRootNavContext.prototype.clone = function () {
        var clone = new StackRootNavContext();
        _.each(this.stack, function (navContext) {
            clone.stack.push(navContext.clone());
        });
        return clone;
    };
    return StackRootNavContext;
}(RootNavContext));
exports.StackRootNavContext = StackRootNavContext;
var TodoRootNavContext = /** @class */ (function (_super) {
    tslib_1.__extends(TodoRootNavContext, _super);
    function TodoRootNavContext(selectedTodoId, showNewTodoPanel) {
        if (showNewTodoPanel === void 0) { showNewTodoPanel = false; }
        var _this = _super.call(this, NavViewId.TodoComposite) || this;
        _this.showNewTodoPanel = showNewTodoPanel;
        _this.todoList = new TodoListViewNavContext(selectedTodoId);
        return _this;
    }
    TodoRootNavContext.prototype.clone = function () {
        return new TodoRootNavContext(this.todoList.selectedTodoId, this.showNewTodoPanel);
    };
    return TodoRootNavContext;
}(CompositeRootNavContext));
exports.TodoRootNavContext = TodoRootNavContext;
//----------------------------------------
// View nav contexts
//----------------------------------------
var ViewNavContext = /** @class */ (function () {
    function ViewNavContext(viewId) {
        this.viewId = viewId;
    }
    return ViewNavContext;
}());
exports.ViewNavContext = ViewNavContext;
var TodoListViewNavContext = /** @class */ (function (_super) {
    tslib_1.__extends(TodoListViewNavContext, _super);
    function TodoListViewNavContext(selectedTodoId) {
        var _this = _super.call(this, NavViewId.TodoList) || this;
        _this.selectedTodoId = selectedTodoId;
        return _this;
    }
    TodoListViewNavContext.prototype.clone = function () {
        return new TodoListViewNavContext(this.selectedTodoId);
    };
    return TodoListViewNavContext;
}(ViewNavContext));
exports.TodoListViewNavContext = TodoListViewNavContext;
var NewTodoViewNavContext = /** @class */ (function (_super) {
    tslib_1.__extends(NewTodoViewNavContext, _super);
    function NewTodoViewNavContext() {
        return _super.call(this, NavViewId.NewTodo) || this;
    }
    NewTodoViewNavContext.prototype.clone = function () {
        return new NewTodoViewNavContext();
    };
    return NewTodoViewNavContext;
}(ViewNavContext));
exports.NewTodoViewNavContext = NewTodoViewNavContext;
var ViewTodoViewNavContext = /** @class */ (function (_super) {
    tslib_1.__extends(ViewTodoViewNavContext, _super);
    function ViewTodoViewNavContext(todoId) {
        var _this = _super.call(this, NavViewId.ViewTodo) || this;
        _this.todoId = todoId;
        return _this;
    }
    ViewTodoViewNavContext.prototype.clone = function () {
        return new ViewTodoViewNavContext(this.todoId);
    };
    return ViewTodoViewNavContext;
}(ViewNavContext));
exports.ViewTodoViewNavContext = ViewTodoViewNavContext;

//# sourceMappingURL=NavModels.js.map
