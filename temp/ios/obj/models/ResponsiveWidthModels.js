"use strict";
/**
* ResponsiveWidthModels.ts
* Copyright: Microsoft 2018
*
* Constants and enumerations used for establishing responsive break points.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var ResponsiveWidth;
(function (ResponsiveWidth) {
    // <= 450
    ResponsiveWidth[ResponsiveWidth["Tiny"] = 0] = "Tiny";
    // 451 - 799
    ResponsiveWidth[ResponsiveWidth["Small"] = 1] = "Small";
    // 800 - 1279
    ResponsiveWidth[ResponsiveWidth["Medium"] = 2] = "Medium";
    // >= 1280
    ResponsiveWidth[ResponsiveWidth["Large"] = 3] = "Large";
})(ResponsiveWidth = exports.ResponsiveWidth || (exports.ResponsiveWidth = {}));
exports.WidthBreakPoints = {
    tiny: 451,
    small: 800,
    medium: 1280
};

//# sourceMappingURL=ResponsiveWidthModels.js.map
