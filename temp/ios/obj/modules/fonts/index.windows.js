"use strict";
/**
* index.windows.ts
* Copyright: Microsoft 2018
*
* Windows implementation of "fonts" module.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var Fonts = /** @class */ (function () {
    function Fonts() {
        this.monospace = 'Courier New';
        this.displayLight = 'Segoe UI';
        this.displayRegular = 'Segoe UI';
        this.displaySemibold = 'Segoe UI';
        this.displayBold = 'Segoe UI';
    }
    return Fonts;
}());
exports.default = new Fonts();

//# sourceMappingURL=index.windows.js.map
