"use strict";
/**
* PageUrlService.tsx
* Copyright: Microsoft 2018
*
* Monitors the NavigationStore and updates the current URL to reflect
* the current deep link location.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var SyncTasks = require("synctasks");
var DeepLinkConverter_1 = require("../app/DeepLinkConverter");
var NavContextStore_1 = require("../stores/NavContextStore");
var PageUrlService = /** @class */ (function () {
    function PageUrlService() {
        this._handlingPopState = false;
        this._navigatingToNewPlace = false;
    }
    PageUrlService.prototype.startup = function () {
        var _this = this;
        NavContextStore_1.default.subscribe(function () { return _this._onNavigationChange(); });
        // Handle back and forward button actions.
        window.addEventListener('popstate', function (e) { return _this._onUrlChange(e); });
        return SyncTasks.Resolved();
    };
    PageUrlService.prototype._onNavigationChange = function () {
        // Prevent reentrancy.
        if (!this._handlingPopState) {
            var navContext = NavContextStore_1.default.getNavContext();
            var newUrl = DeepLinkConverter_1.default.getUrlFromContext(navContext);
            this._navigatingToNewPlace = true;
            window.history.pushState(null, '', newUrl);
            this._navigatingToNewPlace = false;
        }
    };
    PageUrlService.prototype._onUrlChange = function (e) {
        // If the URL is changing because we're programatically changing it, ignore.
        if (this._navigatingToNewPlace) {
            return;
        }
        // Remember that we're popping state. When we navigate, there's no need to push a new state because we're just
        // returning to an existing one on the stack.
        this._handlingPopState = true;
        // Check if we're going back to a previous nav context.
        var navContext = DeepLinkConverter_1.default.getContextFromUrl(window.location.href, NavContextStore_1.default.isUsingStackNav());
        if (navContext) {
            NavContextStore_1.default.setNavContext(navContext);
        }
        // We're done with the navigation change.
        this._handlingPopState = false;
    };
    return PageUrlService;
}());
exports.default = new PageUrlService();

//# sourceMappingURL=PageUrlService.js.map
