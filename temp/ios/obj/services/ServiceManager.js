"use strict";
/**
* ServiceManager.tsx
* Copyright: Microsoft 2018
*
* Coordinates startup of all services. A service is any long-running
* singleton object. Services may depend on other services. The service
* manager will guarantee that all dependent services are started before
* the startup method is called for a service.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var _ = require("lodash");
var SyncTasks = require("synctasks");
var ServiceManager = /** @class */ (function () {
    function ServiceManager() {
    }
    ServiceManager.registerService = function (service, name, dependencies) {
        if (dependencies === void 0) { dependencies = []; }
        if (_.find(ServiceManager._serviceInfos, function (info) { return info.service === service; })) {
            assert.ok(false, 'Duplicate startup registration for object: ' + ServiceManager._getName(service));
            return;
        }
        var serviceInfo = {
            service: service,
            name: name,
            dependencies: dependencies,
            startupPromise: undefined,
            hasBegunStartingUp: false,
            isComplete: false
        };
        ServiceManager._serviceInfos.push(serviceInfo);
    };
    ServiceManager.hasStarted = function (startupable) {
        var startupInfo = _.find(ServiceManager._serviceInfos, function (info) { return info.service === startupable; });
        assert.ok(startupInfo, 'Service not found in hasStarted: ' + ServiceManager._getName(startupable));
        return startupInfo.isComplete;
    };
    ServiceManager.ensureStarted = function (services) {
        return SyncTasks.all(_.map(services, function (service) {
            return ServiceManager.ensureStartedSingle(service);
        })).then(_.noop);
    };
    ServiceManager.ensureStartedSingle = function (service) {
        var foundInfo = _.find(ServiceManager._serviceInfos, function (info) { return info.service === service; });
        if (!foundInfo) {
            assert.ok(false, 'Service not registered for startup: ' + ServiceManager._getName(service));
            return SyncTasks.Rejected('Service not registered for startup: ' +
                ServiceManager._getName(service));
        }
        var startupInfo = foundInfo;
        startupInfo.hasBegunStartingUp = true;
        if (startupInfo.startupPromise) {
            // Startup has begun and/or completed.
            return startupInfo.startupPromise;
        }
        // Pre-wrap this in a promise, since when you async wrap around to cascade
        // dependencies, you need startupPromise to already be set!
        var deferred = SyncTasks.Defer();
        startupInfo.startupPromise = deferred.promise();
        // Make sure all dependencies have launched.
        ServiceManager.ensureStarted(startupInfo.dependencies).then(function () {
            var startupPromise = _.attempt(function () {
                return service.startup();
            });
            if (_.isError(startupPromise)) {
                return SyncTasks.Rejected(startupPromise);
            }
            else {
                return startupPromise;
            }
        }).then(function () {
            startupInfo.isComplete = true;
            deferred.resolve(void 0);
        }, function (err) {
            deferred.reject(err);
        });
        return startupInfo.startupPromise;
    };
    ServiceManager._getName = function (service) {
        var startupInfo = _.find(ServiceManager._serviceInfos, function (info) { return info.service === service; });
        if (startupInfo) {
            return startupInfo.name;
        }
        return 'unknown';
    };
    ServiceManager._serviceInfos = [];
    return ServiceManager;
}());
exports.default = ServiceManager;

//# sourceMappingURL=ServiceManager.js.map
