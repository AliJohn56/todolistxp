"use strict";
/**
* ShimHelpers.ts
* Copyright: Microsoft 2018
*
* Helpers to shim various aspects of the app for React Native.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var SyncTasks = require("synctasks");
var resub_1 = require("resub");
var ExceptionReporter_1 = require("./ExceptionReporter");
function shimEnvironment(isDev, isNative) {
    // Set resub development options early, before autosubscriptions set themselves up.
    resub_1.Options.development = isDev;
    resub_1.Options.preventTryCatchInRender = true;
    // Set SyncTasks exception rules early. We don't want to swallow any exceptions.
    SyncTasks.config.catchExceptions = false;
    SyncTasks.config.exceptionHandler = function (err) {
        if (!err) {
            return;
        }
        assert.fail('Unhandled exception: ' + JSON.stringify(err));
        // tslint:disable-next-line
        throw err;
    };
    SyncTasks.config.unhandledErrorHandler = function (err) {
        assert.fail('Unhandled rejected SyncTask. Error: ' + JSON.stringify(err));
    };
    // Install our exception-reporting alert on local builds.
    var exceptionReporter = new ExceptionReporter_1.default();
    if (isDev) {
        exceptionReporter.registerAlertView();
        exceptionReporter.registerConsoleView();
    }
    if (isNative) {
        shimReactNative();
    }
}
exports.shimEnvironment = shimEnvironment;
// Shim React Native to include various globals found in the browser
// environment like window, document, navigator, etc.
function shimReactNative() {
    if (typeof (document) === 'undefined') {
        global.document = {
            documentElement: {
                style: {}
            }
        };
        global.window.addEventListener = function (eventName) {
            // No-op
        };
        global.window.removeEventListener = function (eventName) {
            // No-op
        };
    }
}

//# sourceMappingURL=ShimHelpers.js.map
