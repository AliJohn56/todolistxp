"use strict";
/**
* AppBootstrapperWeb.tsx
* Copyright: Microsoft 2018
*
* Main entry point for the web app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
// Do shimming before anything else.
var ShimHelpers = require("../utilities/ShimHelpers");
ShimHelpers.shimEnvironment(true, false);
// Initialize AppConfig.
var AppConfig_1 = require("./AppConfig");
var appVersionElement = document.getElementById('appVersion');
var appVersion = appVersionElement.value;
appVersionElement.parentElement.removeChild(appVersionElement);
AppConfig_1.default.initialize({
    appVersion: appVersion
});
var IndexedDbProvider_1 = require("nosqlprovider/dist/IndexedDbProvider");
var InMemoryProvider_1 = require("nosqlprovider/dist/InMemoryProvider");
var WebSqlProvider_1 = require("nosqlprovider/dist/WebSqlProvider");
var SyncTasks = require("synctasks");
var AppBootstrapper_1 = require("./AppBootstrapper");
var AppBootstrapperWeb = /** @class */ (function (_super) {
    tslib_1.__extends(AppBootstrapperWeb, _super);
    function AppBootstrapperWeb() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppBootstrapperWeb.prototype._getDbProvidersToTry = function () {
        // Specify the DB providers that are valid on browser platforms.
        return [
            new IndexedDbProvider_1.IndexedDbProvider(),
            new WebSqlProvider_1.WebSqlProvider(),
            new InMemoryProvider_1.InMemoryProvider()
        ];
    };
    AppBootstrapperWeb.prototype._getInitialUrl = function () {
        return SyncTasks.Resolved(window.location.href);
    };
    return AppBootstrapperWeb;
}(AppBootstrapper_1.default));
exports.default = new AppBootstrapperWeb();

//# sourceMappingURL=AppBootstrapperWeb.js.map
