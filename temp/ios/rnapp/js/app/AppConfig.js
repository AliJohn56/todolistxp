"use strict";
/**
* AppConfig.ts
* Copyright: Microsoft 2018
*
* Provides access to configuration information for the app.
* All of these settings are assumed to be static (set at app
* launch time) throughout the lifetime of the app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var RX = require("reactxp");
var AppConfig = /** @class */ (function () {
    function AppConfig() {
        this._appVersion = '0.0.0.1';
        this._frontendHost = document && document.location ? document.location.host : '';
        this._platformType = RX.Platform.getType();
        this._isTouchInterface = this._platformType === 'ios' || this._platformType === 'android';
        this._startupTime = Date.now();
    }
    AppConfig.prototype.initialize = function (params) {
        if (params.appVersion) {
            this._appVersion = params.appVersion;
        }
    };
    AppConfig.prototype.isDevelopmentBuild = function () {
        return true;
    };
    AppConfig.prototype.getPlatformType = function () {
        return this._platformType;
    };
    AppConfig.prototype.isTouchInterface = function () {
        return this._isTouchInterface;
    };
    AppConfig.prototype.getStartupTime = function () {
        return this._startupTime;
    };
    AppConfig.prototype.getAppVersion = function () {
        return this._appVersion;
    };
    AppConfig.prototype.getFrontendHost = function () {
        return this._frontendHost;
    };
    AppConfig.prototype.getProtocol = function () {
        if (this.getPlatformType() === 'web' &&
            typeof location !== 'undefined' &&
            typeof location.protocol !== 'undefined') {
            return location.protocol;
        }
        return 'https:';
    };
    AppConfig.prototype.getFrontendBaseUrl = function () {
        return this.getProtocol() + '//' + this._frontendHost;
    };
    AppConfig.prototype.getDocRoot = function () {
        return '/';
    };
    AppConfig.prototype.getImagePath = function (imageName) {
        if (imageName === void 0) { imageName = ''; }
        return this.getDocRoot() + 'images/' + imageName;
    };
    return AppConfig;
}());
exports.default = new AppConfig();

//# sourceMappingURL=AppConfig.js.map
