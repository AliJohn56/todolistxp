"use strict";
/**
* Fonts.ts
* Copyright: Microsoft 2018
*
* Common interface for "fonts" module, which handles the fetching
* of all static images.
*/
Object.defineProperty(exports, "__esModule", { value: true });

//# sourceMappingURL=Fonts.js.map
