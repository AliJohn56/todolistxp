"use strict";
/**
* index.web.ts
* Copyright: Microsoft 2018
*
* Web implementation of "fonts" module.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var Fonts = /** @class */ (function () {
    function Fonts() {
        this.monospace = 'monospace';
        this.displayLight = '"SF Semilight", "Segoe System UI Semilight", "Segoe UI Semilight", sans-serif';
        this.displayRegular = '"SF Regular", "Segoe System UI Regular", "Segoe UI Regular", sans-serif';
        this.displaySemibold = '"SF Semibold", "Segoe System UI Semibold", "Segoe UI Semibold", sans-serif';
        this.displayBold = '"SF Bold", "Segoe System UI Bold", "Segoe UI Bold", sans-serif';
    }
    return Fonts;
}());
exports.default = new Fonts();

//# sourceMappingURL=index.web.js.map
