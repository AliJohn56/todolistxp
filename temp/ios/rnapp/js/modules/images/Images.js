"use strict";
/**
* Images.ts
* Copyright: Microsoft 2018
*
* Common interface for "images" module, which handles the fetching
* of all static images.
*/
Object.defineProperty(exports, "__esModule", { value: true });

//# sourceMappingURL=Images.js.map
