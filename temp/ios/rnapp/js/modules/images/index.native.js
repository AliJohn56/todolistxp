"use strict";
/**
* index.native.ts
* Copyright: Microsoft 2018
*
* Native implementation of "images" module.
*/
Object.defineProperty(exports, "__esModule", { value: true });
// The React Native bundler handles resource paths at build time, so they need
// to be specified as full string literals (as opposed to being constructed
// programmatically in a helper method).
// We use accessors and "require" calls to defer loading of these images into
// memory until they are actually used. If we were to require them upfront,
// app launch times would increase substantially.
var ImageSource = /** @class */ (function () {
    function ImageSource() {
    }
    Object.defineProperty(ImageSource.prototype, "todoLogo", {
        get: function () { return require('../../../images/todo-logo.png'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ImageSource.prototype, "todoSmall", {
        get: function () { return require('../../../images/todo-small.png'); },
        enumerable: true,
        configurable: true
    });
    return ImageSource;
}());
exports.default = new ImageSource();

//# sourceMappingURL=index.native.js.map
