"use strict";
/**
* NavContextStore.ts
* Copyright: Microsoft 2018
*
* In-memory singleton store that tracks the current navigation context.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var assert = require("assert");
var RX = require("reactxp");
var resub_1 = require("resub");
var NavActions_1 = require("../app/NavActions");
var NavModels = require("../models/NavModels");
var ResponsiveWidthModels_1 = require("../models/ResponsiveWidthModels");
var ResponsiveWidthStore_1 = require("../stores/ResponsiveWidthStore");
var NavContextStore = /** @class */ (function (_super) {
    tslib_1.__extends(NavContextStore, _super);
    function NavContextStore() {
        var _this = _super.call(this) || this;
        _this._isUsingStackNav = false;
        // Subscribe for changes to the responsive width.
        _this._isUsingStackNav = _this._shouldUseStackNavigation();
        ResponsiveWidthStore_1.default.subscribe(function () {
            var useStackNav = _this._shouldUseStackNavigation();
            if (useStackNav !== _this._isUsingStackNav) {
                _this._isUsingStackNav = useStackNav;
                // Force navigation to the top level. This will also trigger
                // a subscription change event.
                _this.navigateToTodoList();
            }
        });
        if (_this._isUsingStackNav) {
            var stackNavContext = new NavModels.StackRootNavContext();
            stackNavContext.stack.push(new NavModels.TodoListViewNavContext());
            _this._navContext = stackNavContext;
        }
        else {
            _this._navContext = new NavModels.TodoRootNavContext();
        }
        return _this;
    }
    NavContextStore.prototype._shouldUseStackNavigation = function () {
        // Never use stack navigation on desktop platforms.
        var platformType = RX.Platform.getType();
        if (platformType === 'macos' || platformType === 'windows') {
            return false;
        }
        if (ResponsiveWidthStore_1.default.isHeightSmallerThanThresholdNoSubscription(ResponsiveWidthModels_1.WidthBreakPoints.small) &&
            ResponsiveWidthStore_1.default.isWidthSmallerThanThresholdNoSubscription(ResponsiveWidthModels_1.WidthBreakPoints.small)) {
            return true;
        }
        if (ResponsiveWidthStore_1.default.getResponsiveWidth() <= ResponsiveWidthModels_1.ResponsiveWidth.Small) {
            return true;
        }
        return false;
    };
    NavContextStore.prototype.getNavContext = function () {
        return this._navContext;
    };
    NavContextStore.prototype.setNavContext = function (newContext) {
        this._navContext = newContext;
        // Notify all subscribers that the nav context changed.
        this.trigger();
    };
    // Indicates whether the app is currently using stack-based navigation
    // mode or "composite" navigation.
    NavContextStore.prototype.isUsingStackNav = function () {
        return this._isUsingStackNav;
    };
    NavContextStore.prototype.navigateToTodoList = function (selectedTodoId, showNewTodoPanel) {
        if (showNewTodoPanel === void 0) { showNewTodoPanel = false; }
        this.setNavContext(NavActions_1.default.createTodoListContext(this._isUsingStackNav, selectedTodoId, showNewTodoPanel));
    };
    NavContextStore.prototype.popNavigationStack = function () {
        assert.ok(this._navContext.isStackNav);
        var stackContext = this._navContext.clone();
        assert.ok(stackContext.stack.length >= 2);
        stackContext.stack.pop();
        this.setNavContext(stackContext);
    };
    tslib_1.__decorate([
        resub_1.disableWarnings
    ], NavContextStore.prototype, "_shouldUseStackNavigation", null);
    tslib_1.__decorate([
        resub_1.autoSubscribe
    ], NavContextStore.prototype, "getNavContext", null);
    tslib_1.__decorate([
        resub_1.autoSubscribe
    ], NavContextStore.prototype, "isUsingStackNav", null);
    NavContextStore = tslib_1.__decorate([
        resub_1.AutoSubscribeStore
    ], NavContextStore);
    return NavContextStore;
}(resub_1.StoreBase));
exports.NavContextStore = NavContextStore;
exports.default = new NavContextStore();

//# sourceMappingURL=NavContextStore.js.map
