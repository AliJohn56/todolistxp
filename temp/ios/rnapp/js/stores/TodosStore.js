"use strict";
/**
* TodosStore.tsx
* Copyright: Microsoft 2017
*
* Resub Basic Example https://github.com/Microsoft/ReSub
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var _ = require("lodash");
var resub_1 = require("resub");
var LocalDb_1 = require("../app/LocalDb");
var TodosStore = /** @class */ (function (_super) {
    tslib_1.__extends(TodosStore, _super);
    function TodosStore() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._todos = [];
        return _this;
    }
    TodosStore.prototype.startup = function () {
        var _this = this;
        return LocalDb_1.default.getAllTodos().then(function (todos) {
            _this._todos = todos;
        });
    };
    TodosStore.prototype.addTodo = function (todoText) {
        var now = Date.now().valueOf();
        var newTodo = {
            id: now.toString(),
            creationTime: now,
            text: todoText,
            _searchTerms: todoText
        };
        this._todos = this._todos.concat(newTodo);
        // Asynchronously write the new todo item to the DB.
        LocalDb_1.default.putTodo(newTodo);
        this.trigger();
        return newTodo;
    };
    TodosStore.prototype.getTodos = function () {
        return this._todos;
    };
    TodosStore.prototype.getTodoById = function (todoId) {
        return _.find(this._todos, function (todo) { return todo.id === todoId; });
    };
    TodosStore.prototype.deleteTodo = function (todoId) {
        this._todos = _.filter(this._todos, function (todo) { return todo.id !== todoId; });
        // Asynchronously delete the todo item from the DB.
        LocalDb_1.default.deleteTodo(todoId);
        this.trigger();
    };
    tslib_1.__decorate([
        resub_1.autoSubscribe
    ], TodosStore.prototype, "getTodos", null);
    tslib_1.__decorate([
        resub_1.autoSubscribe
    ], TodosStore.prototype, "getTodoById", null);
    TodosStore = tslib_1.__decorate([
        resub_1.AutoSubscribeStore
    ], TodosStore);
    return TodosStore;
}(resub_1.StoreBase));
exports.default = new TodosStore();

//# sourceMappingURL=TodosStore.js.map
