"use strict";
/*
* RootView.tsx
* Copyright: Microsoft 2018
*
* Top-level UI for the TodoList sample app.
*/
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var assert = require("assert");
var _ = require("lodash");
var RX = require("reactxp");
var reactxp_navigation_1 = require("reactxp-navigation");
var resub_1 = require("resub");
var CreateTodoPanel_1 = require("./CreateTodoPanel");
var NavContextStore_1 = require("../stores/NavContextStore");
var NavModels = require("../models/NavModels");
var TodoCompositeView_1 = require("./TodoCompositeView");
var TodoListPanel_1 = require("./TodoListPanel");
var TopBarComposite_1 = require("./TopBarComposite");
var TopBarStack_1 = require("./TopBarStack");
var ViewTodoPanel_1 = require("./ViewTodoPanel");
var _styles = {
    root: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch'
    }),
    stackViewBackground: RX.Styles.createViewStyle({
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: "#fff" /* white */
    })
};
var RootView = /** @class */ (function (_super) {
    tslib_1.__extends(RootView, _super);
    function RootView() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._navigator = null;
        _this._onMountNavigator = function (elem) {
            _this._navigator = elem;
            if (_this._navigator) {
                _this._navigator.immediatelyResetRouteStack(_this._createNavigatorRouteStack(_this.state.navContext));
            }
        };
        _this._onRenderScene = function (navigatorRoute) {
            var viewId = navigatorRoute.routeId;
            var showBackButton = _this._showBackButton(viewId);
            return (RX.createElement(RX.View, { style: _styles.stackViewBackground },
                RX.createElement(TopBarStack_1.default, { title: _this.state.viewTitle, showBackButton: showBackButton, onBack: _this._onBack }),
                _this._renderSceneContents(viewId)));
        };
        _this._onSelectTodoFromList = function (selectedId) {
            NavContextStore_1.default.navigateToTodoList(selectedId, false);
        };
        _this._onCreateNewTodo = function () {
            NavContextStore_1.default.navigateToTodoList(undefined, true);
        };
        _this._onBack = function () {
            if (_this.state.navContext.isStackNav) {
                NavContextStore_1.default.popNavigationStack();
            }
        };
        return _this;
    }
    RootView.prototype._buildState = function (props, initState) {
        var newNavContext = NavContextStore_1.default.getNavContext();
        var partialState = {
            viewTitle: this._getViewTitle(newNavContext),
            navContext: newNavContext
        };
        if (newNavContext.isStackNav) {
            if (this._navigator) {
                var newNavStack = newNavContext;
                var mustResetRouteStack = true;
                if (this.state.navContext && this.state.navContext.isStackNav) {
                    var prevNavStack = this.state.navContext;
                    if (newNavStack.stack.length === prevNavStack.stack.length + 1) {
                        if (this._compareNavStack(newNavStack.stack, prevNavStack.stack, prevNavStack.stack.length)) {
                            this._navigator.push(this._createNavigatorRoute(newNavStack.stack[newNavStack.stack.length - 1].viewId));
                            mustResetRouteStack = false;
                        }
                    }
                    else if (newNavStack.stack.length === prevNavStack.stack.length - 1) {
                        if (this._compareNavStack(newNavStack.stack, prevNavStack.stack, newNavStack.stack.length)) {
                            this._navigator.pop();
                            mustResetRouteStack = false;
                        }
                    }
                }
                if (mustResetRouteStack) {
                    this._navigator.immediatelyResetRouteStack(this._createNavigatorRouteStack(newNavStack));
                }
            }
        }
        return partialState;
    };
    RootView.prototype.render = function () {
        if (this.state.navContext.isStackNav) {
            return (RX.createElement(RX.View, { style: _styles.root, onLayout: this.props.onLayout },
                RX.createElement(reactxp_navigation_1.default, { ref: this._onMountNavigator, renderScene: this._onRenderScene })));
        }
        else {
            var compositeContext = this.state.navContext;
            var showBackButton = this._showBackButton(compositeContext.viewId);
            return (RX.createElement(RX.View, { style: _styles.root, onLayout: this.props.onLayout },
                RX.createElement(TopBarComposite_1.default, { showBackButton: showBackButton, onBack: this._onBack }),
                this._renderMainView()));
        }
    };
    RootView.prototype._showBackButton = function (viewId) {
        return viewId !== NavModels.NavViewId.TodoComposite &&
            viewId !== NavModels.NavViewId.TodoList;
    };
    RootView.prototype._getViewTitle = function (navContext) {
        if (navContext.isStackNav) {
            var stackContext = navContext;
            var topViewId = stackContext.stack[stackContext.stack.length - 1].viewId;
            switch (topViewId) {
                case NavModels.NavViewId.TodoList:
                    return 'Todo List';
                case NavModels.NavViewId.NewTodo:
                    return 'New Todo';
                case NavModels.NavViewId.ViewTodo:
                    return 'Todo Details';
                default:
                    assert.fail('Unknown view');
                    return '';
            }
        }
        else {
            return '';
        }
    };
    RootView.prototype._renderSceneContents = function (viewId) {
        switch (viewId) {
            case NavModels.NavViewId.TodoList:
                return (RX.createElement(TodoListPanel_1.default, { onSelect: this._onSelectTodoFromList, onCreateNew: this._onCreateNewTodo }));
            case NavModels.NavViewId.NewTodo:
                return RX.createElement(CreateTodoPanel_1.default, null);
            case NavModels.NavViewId.ViewTodo:
                var viewContext = this._findNavContextForRoute(viewId);
                if (!viewContext) {
                    return null;
                }
                return RX.createElement(ViewTodoPanel_1.default, { todoId: viewContext.todoId });
            default:
                return undefined;
        }
    };
    RootView.prototype._renderMainView = function () {
        if (this.state.navContext instanceof NavModels.TodoRootNavContext) {
            return RX.createElement(TodoCompositeView_1.default, { navContext: this.state.navContext });
        }
        else {
            assert.fail('Unexpected main view type');
            return null;
        }
    };
    RootView.prototype._createNavigatorRouteStack = function (stackContext) {
        var _this = this;
        return _.map(stackContext.stack, function (viewContext, index) {
            return _this._createNavigatorRoute(viewContext.viewId);
        });
    };
    RootView.prototype._createNavigatorRoute = function (viewId) {
        return {
            routeId: viewId,
            sceneConfigType: reactxp_navigation_1.Types.NavigatorSceneConfigType.FloatFromRight
        };
    };
    RootView.prototype._findNavContextForRoute = function (routeId) {
        assert.ok(this.state.navContext.isStackNav);
        var stackContext = this.state.navContext;
        return _.find(stackContext.stack, function (viewContext) {
            return viewContext.viewId === routeId;
        });
    };
    RootView.prototype._compareNavStack = function (stackA, stackB, count) {
        for (var i = 0; i < count; i++) {
            if (stackA[i].viewId !== stackB[i].viewId) {
                return false;
            }
        }
        return true;
    };
    return RootView;
}(resub_1.ComponentBase));
exports.default = RootView;

//# sourceMappingURL=RootView.js.map
